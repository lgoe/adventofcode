import Base.map;
function map(f)
    return L -> map(f, L)
end

function readinput()
    return readlines("day_15_input.txt") |>
           map(x -> split(x, r"[:, =]")) |>
           map(x -> (x[4], x[7], x[14], x[17])) |>
           map(x -> map(y -> parse(Int, y), x))
end

function solve1()
    y = 2000000
    # y = 10
    input = readinput()
    sensors = [(x[1], x[2]) for x in input]
    beacons = [(x[3], x[4]) for x in input]
    ubeacons = unique(beacons)
    dist(a, b) = abs(a[1] - b[1]) + abs(a[2] - b[2])
    dists = [dist(s, b) for (s, b) in zip(sensors, beacons)]

    minx = minimum(x[1] for x in sensors) - maximum(dists)
    maxx = maximum(x[1] for x in sensors) + maximum(dists)

    counter = 0
    for x in minx:maxx
        impossible = false
        for (i, s) in enumerate(sensors)
            if dist(s, (x, y)) <= dists[i]
                impossible = true
                break
            end
        end
        if impossible
            counter += 1
        end
    end
    return counter - length([b for b in ubeacons if b[2] == y])
end

function solve2()
    input = readinput()
    sensors = [(x[1], x[2]) for x in input]
    beacons = [(x[3], x[4]) for x in input]
    ubeacons = unique(beacons)
    dist(a, b) = abs(a[1] - b[1]) + abs(a[2] - b[2])
    dists = [dist(s, b) for (s, b) in zip(sensors, beacons)]

    candidates = Tuple{Int,Int}[]
    minx = 0
    maxx = 4000000
    miny = 0
    maxy = 4000000
    for (i, s) in enumerate(sensors)
        for k in 0:dists[i]+1
            push!(candidates, (s[1] + k, s[2] + dists[i] + 1 - k))
            push!(candidates, (s[1] - k, s[2] + dists[i] + 1 - k))
            push!(candidates, (s[1] + k, s[2] - dists[i] - 1 + k))
            push!(candidates, (s[1] - k, s[2] - dists[i] - 1 + k))
        end
    end
    filter!(p -> minx <= p[1] <= maxx && miny <= p[2] <= maxy, candidates)

    pos = (-1, -1)
    for p in candidates
        possible = true
        for (i, s) in enumerate(sensors)
            if dist(s, p) <= dists[i]
                possible = false
                break
            end
        end
        if possible
            pos = p
            break
        end
    end
    return pos[1] * 4000000 + pos[2]
end
