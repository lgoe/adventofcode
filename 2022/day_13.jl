function readinput()
    input = Vector[]
    open("day_13_input.txt", "r") do f
        for line in eachline(f)
            if !isempty(line)
                push!(input, eval(Meta.parse(line)))
            end
        end
    end
    return input
end

function issmaller(a::Int, b::Int)
    return a < b
end

function issmaller(a::Int, b::Vector)
    return issmaller([a], b)
end

function issmaller(a::Vector, b::Int)
    return issmaller(a, [b])
end

function issmaller(a::Vector, b::Vector)
    for (x, y) in zip(a, b)
        if issmaller(x, y)
            return true
        elseif issmaller(y, x)
            return false
        end
    end
    if length(a) < length(b)
        return true
    elseif length(a) > length(b)
        return false
    end
    return false
end

function solve1()
    input = readinput()
    return sum(i for i in 1:div(length(input), 2) if issmaller(input[2i-1], input[2i]))
end

function solve2()
    data = readinput()
    div1 = [[2]]
    div2 = [[6]]
    push!(data, div1, div2)
    sort!(data, lt=issmaller)
    i1 = findfirst(x -> x == div1, data)
    i2 = findfirst(x -> x == div2, data)
    return i1 * i2
end
