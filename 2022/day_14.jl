function readinput()
    input = fill('.', 1000, 200)
    maxy = 0
    open("day_14_input.txt") do f
        for line in eachline(f)
            ps = map(x -> (parse(Int, split(x, ',')[1]), parse(Int, split(x, ',')[2])), split(line, " -> "))
            for (p1, p2) in zip(ps, ps[2:end])
                if p1[1] == p2[1]
                    for i in min(p1[2], p2[2]):max(p1[2], p2[2])
                        input[p1[1], i] = '#'
                    end
                else
                    for i in min(p1[1], p2[1]):max(p1[1], p2[1])
                        input[i, p1[2]] = '#'
                    end
                end
                maxy = maximum([maxy, p1[2], p2[2]])
            end
        end
    end
    maxy += 2
    return input, maxy
end

function Base.println(m::Matrix{Char})
    for j in 1:13#1:size(m, 2)
        for i in 450:550#1:size(m, 1)
            print(m[i, j])
        end
        println()
    end
end

solve1() = solve(; hasfloor=false)
solve2() = solve(; hasfloor=true)

function solve(; hasfloor::Bool)
    sandstart = (500, 0)
    data, maxy = readinput()
    if hasfloor
        for i in axes(data, 1)
            data[i, maxy] = '#'
        end
    end
    score = 0
    while true
        sandpos = sandstart
        rests = false
        while true
            if sandpos[2] == maxy
                break
            elseif data[sandpos[1], sandpos[2]+1] == '.' # move down
                sandpos = (sandpos[1], sandpos[2] + 1)
            elseif data[sandpos[1]-1, sandpos[2]+1] == '.' # move left
                sandpos = (sandpos[1] - 1, sandpos[2] + 1)
            elseif data[sandpos[1]+1, sandpos[2]+1] == '.' # move right
                sandpos = (sandpos[1] + 1, sandpos[2] + 1)
            elseif sandpos == sandstart
                score += 1
                break
            else # stop
                data[sandpos[1], sandpos[2]] = 'o'
                rests = true
                score += 1
                break
            end
        end
        # println(data)
        if !rests
            break
        end
    end
    return score
end
