ValveDict = Dict{String,Tuple{Int,Vector{String}}}

function readinput()
    valves = ValveDict()
    open("day_16_input.txt", "r") do f
        for line in eachline(f)
            line = split(line, [' ', '=', ';', ','])
            valves[line[2]] = (parse(Int, line[6]), [String(x) for x in line[12:end] if !isempty(x)])
        end
    end
    return valves
end

function precompute!(valves::ValveDict)
    dist_matrix = Dict{String,Dict{String,Int}}()
    for (curr, data) in valves
        neighbors = data[2]
        dist_matrix[curr] = Dict{String,Int}()
        dist_matrix[curr][curr] = 0
        for neighbor in neighbors
            dist_matrix[curr][neighbor] = 1
        end
    end
    # floyd warshall
    for k in keys(dist_matrix)
        for i in keys(dist_matrix)
            for j in keys(dist_matrix)
                if haskey(dist_matrix[i], k) && haskey(dist_matrix[k], j)
                    dist_matrix[i][j] = min(get(dist_matrix[i], j, typemax(Int)), dist_matrix[i][k] + dist_matrix[k][j])
                end
            end
        end
    end

    for (curr, data) in valves
        if data[1] == 0
            delete!(valves, curr)
            for k in keys(dist_matrix)
                delete!(dist_matrix[k], curr)
            end
        end
    end
    return valves, dist_matrix
end

function solve1()
    valves = readinput()
    valves, dist_matrix = precompute!(valves)
    closed = Set{String}()
    for (curr, data) in valves
        if data[1] != 0
            push!(closed, curr)
        end
    end
    paths = path_dfs(valves, closed, dist_matrix, "AA", 30)
    return maximum([score for (path, score) in paths])
end

function solve2()
    valves = readinput()
    valves, dist_matrix = precompute!(valves)
    closed = Set{String}()
    for (curr, data) in valves
        if data[1] != 0
            push!(closed, curr)
        end
    end
    paths = path_dfs(valves, closed, dist_matrix, "AA", 26)
    pathscores = Dict{Vector{String},Int}()
    for (path, score) in paths
        pathscores[sort(path)] = max(get(pathscores, sort(path), 0), score)
    end
    return maximum([pathscores[p1] + pathscores[p2] for p1 in keys(pathscores), p2 in keys(pathscores) if length(intersect(p1, p2)) == 0])
end

function path_dfs(valves::ValveDict, closed::Set{String}, dist_matrix::Dict{String,Dict{String,Int}}, curr::String, depth::Int)
    paths = Pair{Vector{String},Int}[]
    push!(paths, [] => 0)
    for next in closed
        dist = dist_matrix[curr][next]
        if dist <= depth
            closed_temp = copy(closed)
            delete!(closed_temp, next)
            for (path, score) in path_dfs(valves, closed_temp, dist_matrix, next, depth - dist - 1)
                push!(paths, [next; path] => score + (depth - dist - 1) * valves[next][1])
            end
        end
    end
    return paths
end

