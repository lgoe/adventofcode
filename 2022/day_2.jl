function readinput()
    input = Pair{Char,Char}[]
    open("day_2_input.txt", "r") do f
        for line in eachline(f)
            push!(input, (line[1] => line[3]))
        end
    end
    return input
end

function solve1()
    input = readinput()
    score = 0
    for line in input
        # A for Rock, B for Paper, and C for Scissors
        # X for Rock, Y for Paper, and Z for Scissors
        if line.second == 'X'
            score += 1
            if line.first == 'A'
                score += 3
            elseif line.first == 'C'
                score += 6
            end
        elseif line.second == 'Y'
            score += 2
            if line.first == 'B'
                score += 3
            elseif line.first == 'A'
                score += 6
            end
        elseif line.second == 'Z'
            score += 3
            if line.first == 'C'
                score += 3
            elseif line.first == 'B'
                score += 6
            end
        end
    end
    return score
end

function solve2()
    input = readinput()
    score = 0
    for line in input
        # A for Rock, B for Paper, and C for Scissors
        # X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win
        if line.second == 'X'
            score += 0
            if line.first == 'A'
                score += 3
            elseif line.first == 'B'
                score += 1
            elseif line.first == 'C'
                score += 2
            end
        elseif line.second == 'Y'
            score += 3
            if line.first == 'A'
                score += 1
            elseif line.first == 'B'
                score += 2
            elseif line.first == 'C'
                score += 3
            end
        elseif line.second == 'Z'
            score += 6
            if line.first == 'A'
                score += 2
            elseif line.first == 'B'
                score += 3
            elseif line.first == 'C'
                score += 1
            end
        end
    end
    return score
end

