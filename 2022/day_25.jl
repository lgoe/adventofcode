import Base.map;
function map(f)
    return L -> map(f, L)
end

function readinput()
    return readlines("day_25_input.txt")
end

function solve1()
    readinput() |> map(x -> snafuToDec(x)) |> sum |> decToSnafu
end

function decToSnafu(n::Int)
    if n == 0
        return ""
    end
    lsb = 'x'
    if mod(n, 5) == 0
        lsb = '0'
    elseif mod(n, 5) == 1
        lsb = '1'
        n -= 1
    elseif mod(n, 5) == 2
        lsb = '2'
        n -= 2
    elseif mod(n, 5) == mod(-1, 5)
        lsb = '-'
        n += 1
    else
        lsb = '='
        n += 2
    end
    @assert mod(n, 5) == 0
    return decToSnafu(n ÷ 5) * lsb
end

function snafuToDec(s::String)
    n = 0
    for c in s
        n *= 5
        if c == '1'
            n += 1
        elseif c == '2'
            n += 2
        elseif c == '-'
            n -= 1
        elseif c == '='
            n -= 2
        end
    end
    return n
end
