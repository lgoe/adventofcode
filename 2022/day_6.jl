function readinput()
    open("day_6_input.txt", "r") do f
        return strip(read(f, String))
    end
end

function solve(m::Int)
    stream = readinput()
    for i in m:length(stream)
        if length(unique(stream[i-m+1:i])) == m
            return i
        end
    end
end

solve1() = solve(4)
solve2() = solve(14)
