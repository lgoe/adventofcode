mutable struct Monkey
    items::Vector{Int}
    operation::Function
    test::Int
    testtrue::Int
    testfalse::Int
end

include("day_11_input.jl")

function solve1(monkeys::Dict{Int,Monkey})
    monkeys = deepcopy(monkeys)
    counter = Dict{Int,Int}()
    for _ in 1:20
        for monkey_id in 0:length(monkeys)-1
            monkey = monkeys[monkey_id]
            while !isempty(monkey.items)
                item = popfirst!(monkey.items)
                new_item = div(monkey.operation(item), 3)
                counter[monkey_id] = get(counter, monkey_id, 0) + 1
                if new_item % monkey.test == 0
                    push!(monkeys[monkey.testtrue].items, new_item)
                else
                    push!(monkeys[monkey.testfalse].items, new_item)
                end
            end
        end
    end
    return prod(sort(collect(values(counter)), rev=true)[1:2])
end

function solve2(monkeys::Dict{Int,Monkey})
    monkeys = deepcopy(monkeys)
    counter = Dict{Int,Int}()
    for i in 1:10000
        for monkey_id in 0:length(monkeys)-1
            monkey = monkeys[monkey_id]
            while !isempty(monkey.items)
                item = popfirst!(monkey.items)
                new_item = monkey.operation(item) % prod(m.test for m in values(monkeys))
                counter[monkey_id] = get(counter, monkey_id, 0) + 1
                if new_item % monkey.test == 0
                    push!(monkeys[monkey.testtrue].items, new_item)
                else
                    push!(monkeys[monkey.testfalse].items, new_item)
                end
            end
        end
        # if i == 1 || i == 20 || i % 1000 == 0
        #     println("Round $(i)")
        #     for monkey_id in 0:length(monkeys)-1
        #         println("Monkey $monkey_id has items $(monkeys[monkey_id].items)")
        #     end
        #     println(counter)
        # end
    end
    return prod(sort(collect(values(counter)), rev=true)[1:2])
end
