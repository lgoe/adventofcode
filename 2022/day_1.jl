function readinput()
    input = Vector{Int}[]
    open("day_1_input.txt", "r") do f
        for group in split(read(f, String), "\n\n")
            push!(input, [parse(Int, line) for line in split(group, "\n") if !isempty(line)])
        end
    end
    return input
end

function solve1()
    input = readinput()
    return maximum(map(sum, input))
end

function solve2()
    input = readinput()
    return sum(sort(map(sum, input); rev=true)[1:3])
end
