function readinput()
    return Set((i, j) for (i, line) in enumerate(readlines("day_23_input.txt")) for (j, c) in enumerate(line) if c == '#')
end

function solve1()
    elves = readinput()
    startdir = 0
    for _ in 1:10
        proposed_moves = Dict{Tuple{Int,Int},Tuple{Int,Int}}()
        for (x, y) in elves
            # if isempty(intersect([(x + dx, y + dy) for dx in -1:1 for dy in -1:1 if dx != 0 || dy != 0], elves))
            if !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves) && !((x, y - 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                continue
            end
            if startdir == 0
                if !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x - 1, y)
                elseif !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x + 1, y)
                elseif !((x - 1, y - 1) in elves) && !((x, y - 1) in elves) && !((x + 1, y - 1) in elves)
                    proposed_moves[(x, y)] = (x, y - 1)
                elseif !((x - 1, y + 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x, y + 1)
                end
            elseif startdir == 1
                if !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x + 1, y)
                elseif !((x - 1, y - 1) in elves) && !((x, y - 1) in elves) && !((x + 1, y - 1) in elves)
                    proposed_moves[(x, y)] = (x, y - 1)
                elseif !((x - 1, y + 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x, y + 1)
                elseif !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x - 1, y)
                end
            elseif startdir == 2
                if !((x - 1, y - 1) in elves) && !((x, y - 1) in elves) && !((x + 1, y - 1) in elves)
                    proposed_moves[(x, y)] = (x, y - 1)
                elseif !((x - 1, y + 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x, y + 1)
                elseif !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x - 1, y)
                elseif !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x + 1, y)
                end
            elseif startdir == 3
                if !((x - 1, y + 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x, y + 1)
                elseif !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x - 1, y)
                elseif !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x + 1, y)
                elseif !((x - 1, y - 1) in elves) && !((x, y - 1) in elves) && !((x + 1, y - 1) in elves)
                    proposed_moves[(x, y)] = (x, y - 1)
                end
            end
        end
        for (x, y) in keys(proposed_moves)
            if count(==(proposed_moves[(x, y)]), values(proposed_moves)) == 1
                filter!(!=((x, y)), elves)
                push!(elves, proposed_moves[(x, y)])
            end
        end
        startdir = (startdir + 1) % 4
    end
    minx, maxx = extrema(x for (x, y) in elves)
    miny, maxy = extrema(y for (x, y) in elves)
    return (maxx - minx + 1) * (maxy - miny + 1) - length(elves)
end

function solve2()
    elves = readinput()
    moved = true
    rounds = 0
    startdir = 0
    while moved
        moved = false
        rounds += 1
        if rounds % 10 == 0
            println(rounds)
        end
        proposed_moves = Dict{Tuple{Int,Int},Tuple{Int,Int}}()
        for (x, y) in elves
            # if isempty(intersect([(x + dx, y + dy) for dx in -1:1 for dy in -1:1 if dx != 0 || dy != 0], elves))
            if !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves) && !((x, y - 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                continue
            end
            if startdir == 0
                if !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x - 1, y)
                elseif !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x + 1, y)
                elseif !((x - 1, y - 1) in elves) && !((x, y - 1) in elves) && !((x + 1, y - 1) in elves)
                    proposed_moves[(x, y)] = (x, y - 1)
                elseif !((x - 1, y + 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x, y + 1)
                end
            elseif startdir == 1
                if !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x + 1, y)
                elseif !((x - 1, y - 1) in elves) && !((x, y - 1) in elves) && !((x + 1, y - 1) in elves)
                    proposed_moves[(x, y)] = (x, y - 1)
                elseif !((x - 1, y + 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x, y + 1)
                elseif !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x - 1, y)
                end
            elseif startdir == 2
                if !((x - 1, y - 1) in elves) && !((x, y - 1) in elves) && !((x + 1, y - 1) in elves)
                    proposed_moves[(x, y)] = (x, y - 1)
                elseif !((x - 1, y + 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x, y + 1)
                elseif !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x - 1, y)
                elseif !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x + 1, y)
                end
            elseif startdir == 3
                if !((x - 1, y + 1) in elves) && !((x, y + 1) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x, y + 1)
                elseif !((x - 1, y - 1) in elves) && !((x - 1, y) in elves) && !((x - 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x - 1, y)
                elseif !((x + 1, y - 1) in elves) && !((x + 1, y) in elves) && !((x + 1, y + 1) in elves)
                    proposed_moves[(x, y)] = (x + 1, y)
                elseif !((x - 1, y - 1) in elves) && !((x, y - 1) in elves) && !((x + 1, y - 1) in elves)
                    proposed_moves[(x, y)] = (x, y - 1)
                end
            end
        end
        for (x, y) in keys(proposed_moves)
            if count(==(proposed_moves[(x, y)]), values(proposed_moves)) == 1
                filter!(!=((x, y)), elves)
                push!(elves, proposed_moves[(x, y)])
                moved = true
            end
        end
        startdir = (startdir + 1) % 4

    end
    return rounds
end
