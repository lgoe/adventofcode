function readinput()
    input = Vector{Int}[]
    open("day_8_input.txt", "r") do f
        for line in eachline(f)
            push!(input, [c - '0' for c in line])
        end
    end
    return input
end

function solve1()
    input = readinput()
    n = length(input[1])
    score = 0
    for i in 1:n, j in 1:n
        if isvisiblefromedge(input, i, j, n)
            score += 1
        end
    end
    return score
end

function isvisiblefromedge(input, i, j, n)
    if all(input[k][j] < input[i][j] for k in 1:i-1)
        return true
    end
    if all(input[k][j] < input[i][j] for k in i+1:n)
        return true
    end
    if all(input[i][k] < input[i][j] for k in 1:j-1)
        return true
    end
    if all(input[i][k] < input[i][j] for k in j+1:n)
        return true
    end
    return false
end

function solve2()
    input = readinput()
    n = length(input[1])
    return maximum(scenic_score(input, i, j, n) for i in 1:n, j in 1:n)
end

function scenic_score(input, i, j, n)
    leftscore = 0
    rightscore = 0
    upscore = 0
    downscore = 0
    for k in j-1:-1:1
        leftscore += 1
        if input[i][k] >= input[i][j]
            break
        end
    end
    for k in j+1:n
        rightscore += 1
        if input[i][k] >= input[i][j]
            break
        end
    end
    for k in i-1:-1:1
        upscore += 1
        if input[k][j] >= input[i][j]
            break
        end
    end
    for k in i+1:n
        downscore += 1
        if input[k][j] >= input[i][j]
            break
        end
    end
    return leftscore * rightscore * upscore * downscore
end
