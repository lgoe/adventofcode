import Base.map;
function map(f)
    return L -> map(f, L)
end

function readinput()
    input = Tuple{Int,Int,Int}[]
    open("day_18_input.txt", "r") do f
        for line in eachline(f)
            parts = split(line, ",") |> map(x -> parse(Int, x))
            push!(input, (parts[1], parts[2], parts[3]))
        end
    end
    return input
end

function solve1()
    cubes = readinput()
    counter = 0
    for (x, y, z) in cubes
        if !((x + 1, y, z) in cubes)
            counter += 1
        end
        if !((x - 1, y, z) in cubes)
            counter += 1
        end
        if !((x, y + 1, z) in cubes)
            counter += 1
        end
        if !((x, y - 1, z) in cubes)
            counter += 1
        end
        if !((x, y, z + 1) in cubes)
            counter += 1
        end
        if !((x, y, z - 1) in cubes)
            counter += 1
        end
    end
    return counter
end


function solve2()
    cubes = readinput()
    counter = 0
    minx = minimum(map(x -> x[1] - 1, cubes))
    maxx = maximum(map(x -> x[1] + 1, cubes))
    miny = minimum(map(x -> x[2] - 1, cubes))
    maxy = maximum(map(x -> x[2] + 1, cubes))
    minz = minimum(map(x -> x[3] - 1, cubes))
    maxz = maximum(map(x -> x[3] + 1, cubes))
    outside = (minx, miny, minz)
    todo = Set{Tuple{Int,Int,Int}}([outside])
    done = Set{Tuple{Int,Int,Int}}()
    while !isempty(todo)
        (x, y, z) = pop!(todo)
        if (x, y, z) in done
            continue
        end
        push!(done, (x, y, z))
        if (x, y, z) in cubes
            continue
        end
        if (x + 1, y, z) in cubes
            counter += 1
        end
        if (x - 1, y, z) in cubes
            counter += 1
        end
        if (x, y + 1, z) in cubes
            counter += 1
        end
        if (x, y - 1, z) in cubes
            counter += 1
        end
        if (x, y, z + 1) in cubes
            counter += 1
        end
        if (x, y, z - 1) in cubes
            counter += 1
        end
        if x > minx
            push!(todo, (x - 1, y, z))
        end
        if x < maxx
            push!(todo, (x + 1, y, z))
        end
        if y > miny
            push!(todo, (x, y - 1, z))
        end
        if y < maxy
            push!(todo, (x, y + 1, z))
        end
        if z > minz
            push!(todo, (x, y, z - 1))
        end
        if z < maxz
            push!(todo, (x, y, z + 1))
        end
    end
    return counter

end
