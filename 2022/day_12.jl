function readinput()
    heatmap = Vector{Char}[]
    startpos = (-1, -1)
    endpos = (-1, -1)
    open("day_12_input.txt", "r") do f
        for (i, line) in enumerate(eachline(f))
            mapline = Char[]
            for (j, c) in enumerate(line)
                if c == 'S'
                    startpos = (i, j)
                    push!(mapline, 'a')
                elseif c == 'E'
                    endpos = (i, j)
                    push!(mapline, 'z')
                else
                    push!(mapline, c)
                end
            end
            push!(heatmap, mapline)
        end
    end
    return heatmap, startpos, endpos
end

function solve1()
    heatmap, startpos, endpos = readinput()
    return distance(heatmap, startpos, endpos)
end

function solve2()
    heatmap, _, endpos = readinput()
    aposs = []
    for i in eachindex(heatmap)
        for j in eachindex(heatmap[1])
            if heatmap[i][j] == 'a'
                push!(aposs, (i, j))
            end
        end
    end
    return [distance(heatmap, apos, endpos) for apos in aposs] |> x -> filter(>=(0), x) |> minimum
end

function distance(heatmap::Vector{Vector{Char}}, startpos::Tuple{Int,Int}, endpos::Tuple{Int,Int})
    dists = Dict{Tuple{Int,Int},Int}()
    dists[startpos] = 0
    queue = [startpos]
    while !isempty(queue)
        pos = popfirst!(queue)
        for newpos in neighbors(heatmap, pos)
            if newpos in keys(dists)
                continue
            end
            dists[newpos] = dists[pos] + 1
            push!(queue, newpos)
        end
    end
    return haskey(dists, endpos) ? dists[endpos] : -1
end

function neighbors(heatmap::Vector{Vector{Char}}, pos::Tuple{Int,Int})
    i, j = pos
    n = length(heatmap)
    m = length(heatmap[1])
    return [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)] |>
           x -> filter(p -> 1 <= p[1] <= n && 1 <= p[2] <= m, x) |>
                x -> filter(p -> heatmap[p[1]][p[2]] <= heatmap[pos[1]][pos[2]] + 1, x)
end
