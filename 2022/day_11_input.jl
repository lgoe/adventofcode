############################
# Test input
############################

testmonkeys = Dict{Int,Monkey}()

# Monkey 0:
#   Starting items: 79, 98
#   Operation: new = old * 19
#   Test: divisible by 23
#     If true: throw to monkey 2
#     If false: throw to monkey 3
testmonkeys[0] = Monkey([79, 98], x -> x * 19, 23, 2, 3)

# Monkey 1:
#   Starting items: 54, 65, 75, 74
#   Operation: new = old + 6
#   Test: divisible by 19
#     If true: throw to monkey 2
#     If false: throw to monkey 0
testmonkeys[1] = Monkey([54, 65, 75, 74], x -> x + 6, 19, 2, 0)

# Monkey 2:
#   Starting items: 79, 60, 97
#   Operation: new = old * old
#   Test: divisible by 13
#     If true: throw to monkey 1
#     If false: throw to monkey 3
testmonkeys[2] = Monkey([79, 60, 97], x -> x * x, 13, 1, 3)

# Monkey 3:
#   Starting items: 74
#   Operation: new = old + 3
#   Test: divisible by 17
#     If true: throw to monkey 0
#     If false: throw to monkey 1
testmonkeys[3] = Monkey([74], x -> x + 3, 17, 0, 1)

############################
# Puzzle input
############################

monkeys = Dict{Int,Monkey}()

# Monkey 0:
#   Starting items: 80
#   Operation: new = old * 5
#   Test: divisible by 2
#     If true: throw to monkey 4
#     If false: throw to monkey 3
monkeys[0] = Monkey([80], x -> x * 5, 2, 4, 3)

# Monkey 1:
#   Starting items: 75, 83, 74
#   Operation: new = old + 7
#   Test: divisible by 7
#     If true: throw to monkey 5
#     If false: throw to monkey 6
monkeys[1] = Monkey([75, 83, 74], x -> x + 7, 7, 5, 6)

# Monkey 2:
#   Starting items: 86, 67, 61, 96, 52, 63, 73
#   Operation: new = old + 5
#   Test: divisible by 3
#     If true: throw to monkey 7
#     If false: throw to monkey 0
monkeys[2] = Monkey([86, 67, 61, 96, 52, 63, 73], x -> x + 5, 3, 7, 0)

# Monkey 3:
#   Starting items: 85, 83, 55, 85, 57, 70, 85, 52
#   Operation: new = old + 8
#   Test: divisible by 17
#     If true: throw to monkey 1
#     If false: throw to monkey 5
monkeys[3] = Monkey([85, 83, 55, 85, 57, 70, 85, 52], x -> x + 8, 17, 1, 5)

# Monkey 4:
#   Starting items: 67, 75, 91, 72, 89
#   Operation: new = old + 4
#   Test: divisible by 11
#     If true: throw to monkey 3
#     If false: throw to monkey 1
monkeys[4] = Monkey([67, 75, 91, 72, 89], x -> x + 4, 11, 3, 1)

# Monkey 5:
#   Starting items: 66, 64, 68, 92, 68, 77
#   Operation: new = old * 2
#   Test: divisible by 19
#     If true: throw to monkey 6
#     If false: throw to monkey 2
monkeys[5] = Monkey([66, 64, 68, 92, 68, 77], x -> x * 2, 19, 6, 2)

# Monkey 6:
#   Starting items: 97, 94, 79, 88
#   Operation: new = old * old
#   Test: divisible by 5
#     If true: throw to monkey 2
#     If false: throw to monkey 7
monkeys[6] = Monkey([97, 94, 79, 88], x -> x * x, 5, 2, 7)

# Monkey 7:
#   Starting items: 77, 85
#   Operation: new = old + 6
#   Test: divisible by 13
#     If true: throw to monkey 4
#     If false: throw to monkey 0
monkeys[7] = Monkey([77, 85], x -> x + 6, 13, 4, 0)
