function readinput()
    input = Vector{Int}[]
    open("day_4_input.txt", "r") do f
        for line in eachline(f)
            push!(input, map(x -> parse(Int, x), split(line, [',', '-'])))
        end
    end
    return input
end

function solve1()
    input = readinput()
    score = 0
    for line in input
        if line[1] <= line[3] && line[4] <= line[2]
            score += 1
        elseif line[3] <= line[1] && line[2] <= line[4]
            score += 1
        end
    end
    return score
end

function solve2()
    input = readinput()
    score = 0
    for line in input
        if line[1] <= line[3] <= line[2]
            score += 1
        elseif line[1] <= line[4] <= line[2]
            score += 1
        elseif line[3] <= line[1] <= line[4]
            score += 1
        elseif line[3] <= line[2] <= line[4]
            score += 1
        end
    end
    return score
end
