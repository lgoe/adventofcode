abstract type FSNode end

struct File <: FSNode
    name::String
    size::Int
end


struct Directory <: FSNode
    name::String
    parent::Union{Directory,Nothing}
    children::Vector{FSNode}
end

addFile!(d::Directory, f::File) = push!(d.children, f)
addDir!(d::Directory, name::AbstractString) = push!(d.children, Directory(name, d, []))

findByName(d::Directory, name::AbstractString) = d.children[findfirst(x -> x.name == name, d.children)]

size(f::File) = f.size
size(d::Directory) = sum(map(x -> size(x), d.children))

Base.show(io::IO, f::File) = print(io, "File: $(f.name) ($(f.size) bytes)")
function Base.show(io::IO, d::Directory)
    print(io, "Directory: $(d.name) (")
    show(d.children)
    print(io, ")")
end

function readinput()
    input = Pair{String,String}[]
    open("day_7_input.txt", "r") do f
        for block in split(read(f, String), "\$")
            if !isempty(strip(block))
                push!(input, strip(block)[1:2] => strip(block)[4:end])
            end
        end
    end
    return input
end

function parsecmd(d::Directory, cmd::Pair{String,String})
    if cmd.first == "cd"
        if cmd.second == "/"
            while !isnothing(d.parent)
                d = d.parent
            end
            return d
        elseif cmd.second == ".."
            return d.parent
        else
            return findByName(d, cmd.second)
        end
    elseif cmd.first == "ls"
        for line in split(cmd.second, '\n')
            if !isempty(line)
                parts = split(line, ' ')
                if parts[1] == "dir"
                    addDir!(d, parts[2])
                else
                    addFile!(d, File(parts[2], parse(Int, parts[1])))
                end
            end
        end
        return d
    else
        error("Unknown command: $(cmd.first)")
    end
end

function solve1()
    root = Directory("/", nothing, [])
    dirs = Set{Directory}()

    input = readinput()
    current_dir = root
    for cmd in input
        current_dir = parsecmd(current_dir, cmd)
        push!(dirs, current_dir)
    end
    return sum(size(d) for d in dirs if size(d) <= 100000)
end

function solve2()
    root = Directory("/", nothing, [])
    dirs = Set{Directory}()

    input = readinput()
    current_dir = root
    for cmd in input
        current_dir = parsecmd(current_dir, cmd)
        push!(dirs, current_dir)
    end
    return minimum(size(d) for d in dirs if size(root) - size(d) < 70000000 - 30000000)
end
