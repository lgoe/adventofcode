function readinput()
    input = Dict{String,String}()
    open("day_21_input.txt", "r") do f
        for line in eachline(f)
            parts = split(line, ": ")
            push!(input, parts[1] => '(' * replace(parts[2], "/" => "÷") * ')')
        end
    end
    return input
end

function solve1()
    data = readinput()
    root = "root"
    for k in keys(data)
        for k2 in keys(data)
            if k != k2
                data[k2] = replace(data[k2], k => data[k])
            end
        end
    end
    return eval(Meta.parse(data[root]))
end

function solve2()
    data = readinput()
    data["humn"] = "x"
    root = "root"
    data[root] = split(data[root], " ")[1] * " == " * split(data[root], " ")[end]
    for k in keys(data)
        for k2 in keys(data)
            if k != k2
                data[k2] = replace(data[k2], k => data[k])
            end
        end
    end
    lhs = split(data[root][2:end-1], " == ")[1][2:end-1]
    rhs = split(data[root][2:end-1], " == ")[end][2:end-1]
    if contains(rhs, "x")
        lhs, rhs = rhs, lhs
    end
    ex_lhs = Meta.parse(lhs)
    ex_rhs = Meta.parse(rhs)

    while typeof(ex_lhs) != Symbol
        # println(ex_lhs, " == ", ex_rhs)
        # println(ex_lhs.args)
        if ex_lhs.args[1] == :÷
            ex_lhs, ex_rhs = ex_lhs.args[2], Expr(:call, :*, ex_rhs, ex_lhs.args[3])
        elseif ex_lhs.args[1] == :*
            mult1 = ex_lhs.args[2]
            mult2 = ex_lhs.args[3]
            try
                mult1 = eval(mult1)
            catch UndefVarError
            end
            try
                mult2 = eval(mult2)
            catch UndefVarError
            end
            if isa(mult1, Number)
                ex_lhs, ex_rhs = mult2, Expr(:call, :÷, ex_rhs, mult1)
            elseif isa(mult2, Number)
                ex_lhs, ex_rhs = mult1, Expr(:call, :÷, ex_rhs, mult2)
            end
        elseif ex_lhs.args[1] == :+
            sum1 = ex_lhs.args[2]
            sum2 = ex_lhs.args[3]
            try
                sum1 = eval(sum1)
            catch UndefVarError
            end
            try
                sum2 = eval(sum2)
            catch UndefVarError
            end
            if isa(sum1, Number)
                ex_lhs, ex_rhs = sum2, Expr(:call, :-, ex_rhs, sum1)
            elseif isa(sum2, Number)
                ex_lhs, ex_rhs = sum1, Expr(:call, :-, ex_rhs, sum2)
            end
        elseif ex_lhs.args[1] == :-
            sub1 = ex_lhs.args[2]
            sub2 = ex_lhs.args[3]
            try
                sub1 = eval(sub1)
            catch UndefVarError
            end
            try
                sub2 = eval(sub2)
            catch UndefVarError
            end
            if isa(sub1, Number)
                ex_lhs, ex_rhs = sub2, Expr(:call, :*, (-1), Expr(:call, :-, ex_rhs, sub1))
            elseif isa(sub2, Number)
                ex_lhs, ex_rhs = sub1, Expr(:call, :+, ex_rhs, sub2)
            end
        end
    end
    return eval(ex_rhs)
end
