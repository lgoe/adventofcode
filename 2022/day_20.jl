import Base.map;
function map(f)
    return L -> map(f, L)
end

function readinput()
    return readlines("day_20_input.txt") |>
           map(x -> parse(Int, x))
end

function solve1()
    data = collect(enumerate(readinput()))
    order = copy(data)

    # mixing
    for elem in order
        i = findfirst(==(elem), data)
        data = vcat(data[i+1:end], data[1:i-1])
        newi = mod(elem[2], length(data)) + 1
        data = vcat(data[1:newi-1], elem, data[newi:end])
    end

    # reading result
    score = 0
    i = findfirst(x -> x[2] == 0, data)
    data = vcat(data[i+1:end], data[1:i])
    for i in [1000, 2000, 3000]
        newi = mod(i - 1, length(data)) + 1
        score += data[newi][2]
    end

    return score
end

function solve2()
    data = collect(map(x -> (x[1], 811589153 * x[2]), enumerate(readinput())))
    order = copy(data)

    # mixing
    for _ in 1:10
        for elem in order
            i = findfirst(==(elem), data)
            data = vcat(data[i+1:end], data[1:i-1])
            newi = mod(elem[2], length(data)) + 1
            data = vcat(data[1:newi-1], elem, data[newi:end])
        end
    end

    # reading result
    score = 0
    i = findfirst(x -> x[2] == 0, data)
    data = vcat(data[i+1:end], data[1:i])
    for i in [1000, 2000, 3000]
        newi = mod(i - 1, length(data)) + 1
        score += data[newi][2]
    end

    return score
end
