function readinput()
    input = Tuple{Symbol,Int}[]
    open("day_9_input.txt", "r") do f
        for line in eachline(f)
            push!(input, (Symbol(line[1]), parse(Int, line[3:end])))
        end
    end
    return input
end

solve1() = solve(2)
solve2() = solve(10)

function solve(m::Int)
    input = readinput()
    pos = [(0, 0) for _ in 1:m]
    history = Set{Tuple{Int,Int}}()
    push!(history, pos[end])
    for (dir, n) in input
        for _ in 1:n
            if dir == :U
                pos[1] = (pos[1][1] + 1, pos[1][2])
            elseif dir == :D
                pos[1] = (pos[1][1] - 1, pos[1][2])
            elseif dir == :L
                pos[1] = (pos[1][1], pos[1][2] - 1)
            elseif dir == :R
                pos[1] = (pos[1][1], pos[1][2] + 1)
            end
            for i in 1:m-1
                movenext(pos, i, i + 1)
            end
            push!(history, pos[end])
        end
    end
    return length(history)
end

function movenext(pos, head, tail)
    if pos[head][1] - pos[tail][1] > 1
        if pos[head][2] < pos[tail][2]
            pos[tail] = (pos[tail][1] + 1, pos[tail][2] - 1)
        elseif pos[head][2] > pos[tail][2]
            pos[tail] = (pos[tail][1] + 1, pos[tail][2] + 1)
        else
            pos[tail] = (pos[tail][1] + 1, pos[tail][2])
        end
    end

    if pos[head][1] - pos[tail][1] < -1
        if pos[head][2] < pos[tail][2]
            pos[tail] = (pos[tail][1] - 1, pos[tail][2] - 1)
        elseif pos[head][2] > pos[tail][2]
            pos[tail] = (pos[tail][1] - 1, pos[tail][2] + 1)
        else
            pos[tail] = (pos[tail][1] - 1, pos[tail][2])
        end
    end

    if pos[head][2] - pos[tail][2] < -1
        if pos[head][1] < pos[tail][1]
            pos[tail] = (pos[tail][1] - 1, pos[tail][2] - 1)
        elseif pos[head][1] > pos[tail][1]
            pos[tail] = (pos[tail][1] + 1, pos[tail][2] - 1)
        else
            pos[tail] = (pos[tail][1], pos[tail][2] - 1)
        end
    end

    if pos[head][2] - pos[tail][2] > 1
        if pos[head][1] < pos[tail][1]
            pos[tail] = (pos[tail][1] - 1, pos[tail][2] + 1)
        elseif pos[head][1] > pos[tail][1]
            pos[tail] = (pos[tail][1] + 1, pos[tail][2] + 1)
        else
            pos[tail] = (pos[tail][1], pos[tail][2] + 1)
        end
    end
end
