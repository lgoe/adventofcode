function readinput()
    input = Union{Nothing,Int}[]
    open("day_10_input.txt", "r") do f
        for line in eachline(f)
            if line == "noop"
                push!(input, nothing)
            elseif line[1:4] == "addx"
                push!(input, parse(Int, line[5:end]))
            end
        end
    end
    return input
end

function run()
    input = readinput()
    x = 1
    xs = Int[]
    for i in input
        if i === nothing
            push!(xs, x)
        else
            push!(xs, x)
            push!(xs, x)
            x += i
        end
    end
    return xs
end

function solve1()
    xs = run()
    return sum(i * xs[i] for i in 20:40:220)
end

function solve2()
    xs = run()
    for i in 1:240
        if -1 <= xs[i] <= 40 && abs((i % 40) - 1 - xs[i]) <= 1
            print("#")
        else
            print(" ")
        end
        if i % 40 == 0
            println()
        end
    end
end
