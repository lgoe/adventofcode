import Base.map;
function map(f)
    return L -> map(f, L)
end

function readinput()
    input = NTuple{7,Int}[]
    open("day_19_input.txt", "r") do f
        for line in eachline(f)
            parts = split(line, [' ', ':'])
            data = (parts[2], parts[8], parts[14], parts[20], parts[23], parts[29], parts[32]) |> map(x -> parse(Int, x))
            push!(input, data)
        end
    end
    return input
end

function solve1()
    input = readinput()
    score = 0
    for (i, line) in enumerate(input)
        bpnr, orerob_ore, clayrob_ore, obsrob_ore, obsrob_clay, georob_ore, georob_obs = line
        robs = [1, 0, 0, 0]
        ress = [0, 0, 0, 0]
        orerob_costs = [orerob_ore, 0, 0, 0]
        clayrob_costs = [clayrob_ore, 0, 0, 0]
        obsrob_costs = [obsrob_ore, obsrob_clay, 0, 0]
        georob_costs = [georob_ore, 0, georob_obs, 0]

        ngeodes = dfs(robs, ress + robs, 24, orerob_costs, clayrob_costs, obsrob_costs, georob_costs)
        println(ngeodes)
        score += i * ngeodes
    end
    return score
end

function solve2()
    input = readinput()
    score = 1
    for (i, line) in enumerate(input)
        if i in [1, 2, 3]
            bpnr, orerob_ore, clayrob_ore, obsrob_ore, obsrob_clay, georob_ore, georob_obs = line
            robs = [1, 0, 0, 0]
            ress = [0, 0, 0, 0]
            orerob_costs = [orerob_ore, 0, 0, 0]
            clayrob_costs = [clayrob_ore, 0, 0, 0]
            obsrob_costs = [obsrob_ore, obsrob_clay, 0, 0]
            georob_costs = [georob_ore, 0, georob_obs, 0]

            ngeodes = dfs(robs, ress + robs, 32, orerob_costs, clayrob_costs, obsrob_costs, georob_costs)
            println(ngeodes)
            score *= ngeodes
        end
    end
    return score
end

function dfs(robs, ress, depth, orerob_costs, clayrob_costs, obsrob_costs, georob_costs, bestscore=0)

    if depth == 1
        return ress[4]
    end
    if depth == 0
        return 0
    end
    if ress[4] + (depth - 1) * robs[4] + div((depth - 1) * (depth - 2), 2) < bestscore
        return 0
    end
    score = ress[4] + (depth - 1) * robs[4]

    # buy orerob
    if depth >= 15 && robs[1] < max(clayrob_costs[1], obsrob_costs[1], georob_costs[1])
        i = minimum(i for i in 0:depth-1 if all(>=(0), ress + i * robs - orerob_costs); init=typemax(Int))
        if i < depth
            score = max(score, dfs(robs + [1, 0, 0, 0], ress + (i + 1) * robs - orerob_costs, depth - i - 1, orerob_costs, clayrob_costs, obsrob_costs, georob_costs, score))
        end
    end

    # buy clayrob
    if depth >= 8 && robs[2] < max(orerob_costs[2], obsrob_costs[2], georob_costs[2])
        i = minimum(i for i in 0:depth-1 if all(>=(0), ress + i * robs - clayrob_costs); init=typemax(Int))
        if i < depth
            score = max(score, dfs(robs + [0, 1, 0, 0], ress + (i + 1) * robs - clayrob_costs, depth - i - 1, orerob_costs, clayrob_costs, obsrob_costs, georob_costs, score))
        end
    end

    # buy obsrob
    if depth >= 3
        if robs[3] < max(orerob_costs[3], clayrob_costs[3], georob_costs[3])
            i = minimum(i for i in 0:depth-1 if all(>=(0), ress + i * robs - obsrob_costs); init=typemax(Int))
            if i < depth
                score = max(score, dfs(robs + [0, 0, 1, 0], ress + (i + 1) * robs - obsrob_costs, depth - i - 1, orerob_costs, clayrob_costs, obsrob_costs, georob_costs, score))
            end
        end
    end

    # buy georob
    i = minimum(i for i in 0:depth-1 if all(>=(0), ress + i * robs - georob_costs); init=typemax(Int))
    if i < depth
        score = max(score, dfs(robs + [0, 0, 0, 1], ress + (i + 1) * robs - georob_costs, depth - i - 1, orerob_costs, clayrob_costs, obsrob_costs, georob_costs, score))
    end

    return score

end

