str = """
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 """

function readinput()
    config = Vector{Char}[]
    instructions = Tuple{Int,Int,Int}[]
    open("day_5_input.txt", "r") do f
        part1, part2 = split(read(f, String), "\n\n")

        for line in split(part2, '\n')
            if isempty(line)
                continue
            end
            segs = split(line)
            push!(instructions, parse.(Int, (segs[2], segs[4], segs[6])))
        end

        lines = split(part1, '\n')
        nstacks = length(split(lines[end]))
        for i in 1:nstacks
            stack = Vector{Char}()
            for line in lines[end-1:-1:1]
                if line[4i-2] == ' '
                    break
                end
                push!(stack, line[4i-2])
            end
            push!(config, stack)
        end
    end

    return config, instructions
end

function solve1()
    config, instructions = readinput()
    for i in instructions
        config = apply_instruction(config, i; keep_order=false)
    end
    return join(map(last, config))
end

function solve2()
    config, instructions = readinput()
    for i in instructions
        config = apply_instruction(config, i; keep_order=true)
    end
    return join(map(last, config))
end


function apply_instruction(config, instruction; keep_order)
    n, from, to = instruction
    if keep_order
        crates = config[from][end-n+1:end]
        config[from] = config[from][1:end-n]
        config[to] = vcat(config[to], crates)
    else
        for _ in 1:n
            crate = pop!(config[from])
            push!(config[to], crate)
        end
    end
    return config
end
