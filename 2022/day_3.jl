function readinput()
    input = String[]
    open("day_3_input.txt", "r") do f
        for line in eachline(f)
            push!(input, line)
        end
    end
    return input
end

function priority(c::Char)
    if 'a' <= c <= 'z'
        return c - 'a' + 1
    elseif 'A' <= c <= 'Z'
        return c - 'A' + 27
    else
        error("")
    end
end

function solve1()
    input = readinput()
    return sum(backpack -> sum(
            priority,
            intersect(
                backpack[1:div(length(backpack), 2)],
                backpack[div(length(backpack), 2)+1:end])),
        input)
end

function solve2()
    input = readinput()
    return sum(priority(intersect(input[i], input[i+1], input[i+2])[1]) for i in 1:3:length(input))
end
