module Main where

-- import Data.List.Split

import           Data.Char                    (isAlphaNum, isDigit)
import           Data.List                    (groupBy)
import           Data.Maybe                   (catMaybes, listToMaybe)
import           Text.ParserCombinators.ReadP (ReadP, char, choice, eof, munch1,
                                               readP_to_S, sepBy1, skipSpaces,
                                               string)

data Property = BYR | IYR | EYR | HGT | HCL | ECL | PID | CID deriving (Show, Eq)

type Passport = [(Property, String)]

main :: IO ()
main = do
    ps <- readInput "day_4_input.txt"
    print $ solve1 ps
    print $ solve2 ps

fAnd :: [a -> Bool] -> a -> Bool
fAnd = fmap and . sequence

fOr :: [a -> Bool] -> a -> Bool
fOr = fmap or . sequence

-- readInput :: FilePath -> [Passport]
-- readInput path = map readPassport . (split . dropFinalBlank . drppDelims . onSubstring "\n\n") <$> readFile path

readInput :: FilePath -> IO [Maybe Passport]
readInput path = map (readPassport . unlines) . groupBy (const (/= "")) . lines <$> readFile path

-- >>> readPassport "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\nbyr:1937 iyr:2017 cid:147 hgt:183cm"
-- Just [(ECL,"gry"),(PID,"860033327"),(EYR,"2020"),(HCL,"#fffffd"),(BYR,"1937"),(IYR,"2017"),(CID,"147"),(HGT,"183cm")]

readPassport :: String -> Maybe Passport
readPassport = (fst <$>) . listToMaybe . readP_to_S passportReader

passportReader :: ReadP Passport
passportReader = do
    skipSpaces
    pp <- sepBy1 propReader $ choice [char ' ', char '\n']
    skipSpaces
    eof
    return pp

propReader :: ReadP (Property, String)
propReader = do
    prop <-
        choice
            [ string "byr" >>= const (return BYR)
            , string "iyr" >>= const (return IYR)
            , string "eyr" >>= const (return EYR)
            , string "hgt" >>= const (return HGT)
            , string "hcl" >>= const (return HCL)
            , string "ecl" >>= const (return ECL)
            , string "pid" >>= const (return PID)
            , string "cid" >>= const (return CID)
            ]
    char ':'
    val <- munch1 (\c -> any ($c) [isAlphaNum, (== '#')])
    return (prop, val)

isComplete :: Passport -> Bool
isComplete p = all (`elem` map fst p) [BYR, IYR, EYR, HGT, HCL, ECL, PID]

isValid :: Passport -> Bool
isValid = all isValid'

--  where
isValid' :: (Property, String) -> Bool
isValid' (BYR, byr) = length byr == 4 && all isDigit byr && 1920 <= read byr && read byr <= 2002
isValid' (IYR, iyr) = length iyr == 4 && all isDigit iyr && 2010 <= read iyr && read iyr <= 2020
isValid' (EYR, eyr) = length eyr == 4 && all isDigit eyr && 2020 <= read eyr && read eyr <= 2030
isValid' (HGT, hgt) = (drop 3 hgt == "cm" && 150 <= read (take 3 hgt) && read (take 3 hgt) <= 193) || (drop 2 hgt == "in" && 59 <= read (take 2 hgt) && read (take 2 hgt) <= 76)
isValid' (HCL, hcl) = length hcl == 7 && head hcl == '#' && all (fOr [isDigit, (`elem` ['a', 'b', 'c', 'd', 'e', 'f'])]) (tail hcl)
isValid' (ECL, ecl) = ecl `elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
isValid' (PID, pid) = length pid == 9 && all isDigit pid
isValid' (CID, _) = True

solve1 :: [Maybe Passport] -> Int
solve1 = length . filter isComplete . catMaybes

solve2 :: [Maybe Passport] -> Int
solve2 = length . filter (fAnd [isComplete, isValid]) . catMaybes
