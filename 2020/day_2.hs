module Main where

import           Data.Char                    (isDigit)
import           Data.Maybe                   (listToMaybe, mapMaybe)
import           Text.ParserCombinators.ReadP (ReadP, char, eof, munch1,
                                               readP_to_S, satisfy, string)

type Line = (Int, Int, Char, String)

main = do
  lines <- readData "day_2_input.txt"
  print $ solve1 lines
  print $ solve2 lines

-- >>> readLine "1-3 b: cdefg"
-- Just (1,3,'b',"cdefg")

readData :: FilePath -> IO [Line]
readData path = mapMaybe readLine . lines <$> readFile path

readLine :: String -> Maybe Line
readLine = (fst <$>) . listToMaybe . readP_to_S lineReader

lineReader :: ReadP Line
lineReader = do
  a <- munch1 isDigit
  char '-'
  b <- munch1 isDigit
  char ' '
  c <- satisfy (const True)
  string ": "
  pw <- munch1 (const True)
  eof
  return (read a, read b, c, pw)

solve1 :: [Line] -> Int
solve1 = length . filter (\(mini, maxi, c, pw) -> mini <= length (filter (c ==) pw) && maxi >= length (filter (c ==) pw))

solve2 :: [Line] -> Int
solve2 = length . filter (\(a, b, c, pw) -> (c == pw !! (a -1)) /= (c == pw !! (b -1)))
