module Main where

import           Control.Applicative          (liftA2)
import           Data.Char                    (isAlpha, isDigit, isSpace)
import           Data.Graph                   (Graph, Vertex, graphFromEdges, reachable, transposeG)
import           Data.List                    ((\\))
import           Data.Maybe                   (fromMaybe, listToMaybe, mapMaybe)
import           Text.ParserCombinators.ReadP (ReadP, char, choice, many, munch1, optional, readP_to_S, satisfy, sepBy1,
                                               string)

type Bag = String

main = do
  bags <- readInput "day_7_input.txt"
  let (graph, nodeFromVertex, vertexFromKey) = graphFromEdges $ map (\(i, o) -> (i, i, map snd o)) bags
  print $ solve1 graph $ fromMaybe 0 $ vertexFromKey "shiny gold"
  print $ solve2 bags "shiny gold"
  return ()

readInput :: FilePath -> IO [(Bag, [(Int, Bag)])]
readInput path = mapMaybe (listToMaybe . map fst . readP_to_S lineReader) . lines <$> readFile path

lineReader :: ReadP (Bag, [(Int, Bag)])
lineReader = do
  outer <- bagReader
  string " contain "
  inner <-
    choice
      [ sepBy1
          ( do
              num <- read <$> munch1 isDigit
              char ' '
              inner <- bagReader
              return (num, inner)
          )
          (string ", "),
        string "no other bags" >>= const (return [])
      ]
  char '.'
  return (outer, inner)

bagReader :: ReadP Bag
bagReader = do
  color <- many $ satisfy (liftA2 (||) isAlpha isSpace)
  string " bag"
  optional $ char 's'
  return color

solve1 :: Graph -> Vertex -> Int
solve1 g v = length $ reachable (transposeG g) v \\ [v]

reduce :: [(Bag, [(Int, Bag)])] -> (Int, [(Int, Bag)]) -> (Int, [(Int, Bag)])
reduce g (num, (i, b) : bs) = (num + i, mapFirst (* i) xs ++ bs)
  where
    mapFirst :: (a -> c) -> [(a, b)] -> [(c, b)]
    mapFirst _ []              = []
    mapFirst f ((x, y) : rest) = (f x, y) : mapFirst f rest

    (x, xs) = head $ filter ((b ==) . fst) g

solve2 :: [(Bag, [(Int, Bag)])] -> Bag -> Int
solve2 g b = (+ (-1)) $ fst $ head $ dropWhile (not . null . snd) $ iterate (reduce g) (0, [(1, b)])
