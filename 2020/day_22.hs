{-# LANGUAGE TypeApplications #-}

module Main where

import Data.Either (fromLeft, isRight)
import Data.Function (on)
import Data.List (groupBy)
import Data.Maybe (isJust, mapMaybe)
import Text.Read (readMaybe)

main :: IO ()
main = do
  file <- readFile "day_22_input.txt"
  let decks = (\xs -> (head xs, head $ tail xs)) $ mapMaybe sequence $ groupBy ((==) `on` isJust) $ map (readMaybe @Int) $ lines file
  -- print $ solve1 decks
  print $ solve2 decks

type Deck a = [a]

data Player = Me | Crab deriving (Show, Eq)

score :: Integral a => [a] -> a
score = sum . zipWith (*) [1 ..] . reverse

playRound1 :: Ord a => (Deck a, Deck a) -> Either (Player, Deck a) (Deck a, Deck a)
playRound1 ([], ys) = Left (Crab, ys)
playRound1 (xs, []) = Left (Me, xs)
playRound1 (x : xs, y : ys) = Right $ if x > y then (xs ++ [x, y], ys) else (xs, ys ++ [y, x])

winner1 :: (Ord a) => (Deck a, Deck a) -> (Player, Deck a)
winner1 = fromLeft undefined . head . dropWhile isRight . iterate (>>= playRound1) . Right

solve1 :: (Ord a, Integral a) => (Deck a, Deck a) -> a
solve1 = score . snd . winner1

playRound2 :: ((Deck Int, Deck Int), [(Deck Int, Deck Int)]) -> Either (Player, Deck Int) ((Deck Int, Deck Int), [(Deck Int, Deck Int)])
playRound2 (([], ys), _) = Left (Crab, ys)
playRound2 ((xs, []), _) = Left (Me, xs)
playRound2 ((x : xs, y : ys), ss) = if (x : xs, y : ys) `elem` ss then Left (Me, x : xs) else Right (next, (x : xs, y : ys) : ss)
  where
    next
      | length xs >= x && length ys >= y =
        if Me == fst ( winner2 (take x xs, take y ys))
          then (xs ++ [x, y], ys)
          else (xs, ys ++ [y, x])
      | x > y = (xs ++ [x, y], ys)
      | otherwise = (xs, ys ++ [y, x])

winner2 :: (Deck Int, Deck Int) -> (Player, Deck Int)
winner2 = fromLeft undefined . head . dropWhile isRight . iterate (>>= playRound2) . Right . flip (,) []

solve2 :: (Deck Int, Deck Int) -> Int
solve2 = score . snd . winner2
