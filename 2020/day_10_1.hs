module Main where

import Data.List ( group, sort )

main :: IO Int
main = product . solve1 . map read . lines <$> readFile "day_10_input.txt"

solve1 :: [Int] -> [Int]
solve1 x = map length $ group $ sort $ zipWith (-) (y ++ [maximum y + 3]) (0:y)
  where
    y = sort x
