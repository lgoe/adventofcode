{-# LANGUAGE LambdaCase #-}

module Main where

data Square = Empty | Tree deriving (Show)

isTree :: Square -> Bool
isTree Empty = False
isTree Tree  = True

main :: IO ()
main = do
  sqss <- readData "day_3_input.txt"
  print $ solve1 sqss 3 1
  print $ solve2 sqss


readData :: FilePath -> IO [[Square]]
readData path = map (map (\case '.' -> Empty; '#' -> Tree)) . lines <$> readFile path

solve1 :: [[Square]] -> Int -> Int -> Int
solve1 sqss r d = length $ filter isTree $ [(sqss !! i) !! j | i <- [0 .. length sqss -1], i `mod` d == 0, j <- [0 .. length (sqss !! i) -1], j == r * (i `div` d) `mod` length (sqss !! i)]

solve2 :: [[Square]] -> Int
solve2 sqss = product $ map (uncurry $ solve1 sqss) [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
