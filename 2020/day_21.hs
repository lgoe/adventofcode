module Main where

import Data.Char
import Data.List
import Data.Maybe
import Text.ParserCombinators.ReadP

type Ingredient = String

type Allergen = String

type Food = ([Ingredient], [Allergen])

main :: IO ()
main = do
  foods <- readFoods "day_21_input.txt"
  print $ solve1 foods
  print $ solve2 foods

-- >>> readFood "trh fvjkl sbzzf mxmxvkd (contains dairy)"
-- Just (["trh","fvjkl","sbzzf","mxmxvkd"],["dairy"])

readFoods :: FilePath -> IO [Food]
readFoods path = mapMaybe readFood . lines <$> readFile path

readFood :: String -> Maybe Food
readFood = (fst <$>) . listToMaybe . readP_to_S foodReader

foodReader :: ReadP Food
foodReader = do
  ingredients <- sepBy (munch1 isAlpha) (char ' ')
  skipSpaces
  allergens <- between (char '(') (char ')') $ do
    string "contains"
    skipSpaces
    sepBy (munch1 isAlpha) (do skipSpaces; char ','; skipSpaces)
  return (ingredients, allergens)

allergensUniq :: Ord b => [([a], [b])] -> [b]
allergensUniq = nub . concatMap snd

isCandidate :: (Eq a, Ord b) => [([a], [b])] -> a -> Bool
isCandidate fs a = any (isCandidateFor fs a) $ allergensUniq fs

isCandidateFor :: (Eq a, Ord b) => [([a], [b])] -> a -> b -> Bool
isCandidateFor fs a b = all ((a `elem`) . fst) $ filter ((b `elem`) . snd) fs

solve1 :: [Food] -> Int
solve1 fs = length $ filter (not . isCandidate fs) $ concatMap fst fs

solve2 :: [Food] -> String
solve2 fs = intercalate "," $ deconflict $ map (\b -> [a | a <- nub $ concatMap fst fs, isCandidateFor fs a b]) $ sort $ allergensUniq fs

isSingleton :: [a] -> Bool
isSingleton [_] = True
isSingleton _ = False


deconflict :: [[String]] -> [String]
deconflict xss
  | all isSingleton xss = map head xss
  | otherwise = deconflict $ map (\xs -> if isSingleton xs then xs else xs \\ fixed) xss
  where
    fixed = map head $ filter isSingleton xss
