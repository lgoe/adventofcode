{-# LANGUAGE LambdaCase #-}

module Main where

import Data.Char (isDigit)
import Data.List (unfoldr)

data MaskVal = Bool Bool | Ignore deriving (Show)
type Mask = [MaskVal]
type Value = [Bool]

(...) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(...) = (.) . (.)

applyMask1 :: Mask -> Value -> Value
applyMask1 = zipWith f
 where
  f :: MaskVal -> Bool -> Bool
  f Ignore = id
  f (Bool b) = const b

applyMask2 :: Mask -> Value -> [Value]
applyMask2 = foldr g [[]] ... zip
 where
  g :: (MaskVal, Bool) -> [Value] -> [Value]
  g (Bool False, b) = map (b :)
  g (Bool True, _) = map (True :)
  g (Ignore, _) = \xs -> concatMap ($xs) [map (False :), map (True :)]

parseMask :: String -> Mask
parseMask =
  map
    ( \case
        'X' -> Ignore
        '0' -> Bool False
        '1' -> Bool True
    )

bool2int :: Value -> Integer
bool2int = foldl (\x y -> 2 * x + if y then 1 else 0) 0

int2bool :: Integer -> Value
int2bool = f . reverse . map (\case 0 -> False; 1 -> True) . unfoldr (\x -> if x == 0 then Nothing else Just (x `mod` 2, x `div` 2))
 where
  f x
    | length x < 36 = f $ False : x
    | otherwise = x

-- >>> parseLine "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"
-- Left [Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Ignore,Bool True,Ignore,Ignore,Ignore,Ignore,Bool False,Ignore]

-- >>> parseLine "mem[8] = 11"
-- Right (8,11)

parseLine :: String -> Either Mask (Integer, Integer)
parseLine s
  | take 4 s == "mask" = Left $ parseMask $ drop 7 s
  | take 3 s == "mem" = Right (read $ takeWhile isDigit $drop 4 s, read $ drop 2 $ dropWhile (/= '=') s)

eval1 :: Mask -> [(Integer, Integer)] -> [String] -> [(Integer, Integer)]
eval1 m d = snd . foldl f (m, d)
 where
  f :: (Mask, [(Integer, Integer)]) -> String -> (Mask, [(Integer, Integer)])
  f (m, d) s = case parseLine s of
    Left m' -> (m', d)
    Right (x, y) -> (m, d')
     where
      d' = (x, bool2int $ applyMask1 m $ int2bool y) : filter ((/= x) . fst) d

solve1 :: String -> Integer
solve1 = sum . map snd . eval1 [] [] . lines

eval2 :: Mask -> [(Integer, Integer)] -> [String] -> [(Integer, Integer)]
eval2 m d = snd . foldl f (m, d)
 where
  f :: (Mask, [(Integer, Integer)]) -> String -> (Mask, [(Integer, Integer)])
  f (m, d) s = case parseLine s of
    Left m' -> (m', d)
    Right (x, y) -> (m, d')
     where
      xs = map bool2int $ applyMask2 m $ int2bool x
      d' = [(x', y) | x' <- xs] ++ filter ((`notElem` xs) . fst) d

solve2 :: String -> Integer
solve2 = sum . map snd . eval2 [] [] . lines

main = do
  input <- readFile "day_14_input.txt"
  print $ solve1 input
  print $ solve2 input
