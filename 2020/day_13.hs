module Main where

import           Data.Function (on)
import           Data.List     (minimumBy)
import           Data.Maybe    (catMaybes, fromMaybe, isJust)
import qualified Data.Text     as T
import           Text.Read     (readMaybe)

main = do
    input <- lines <$> readFile "day_13_input.txt"
    let time = read $ head input
    let ids = map (readMaybe . T.unpack) $ T.split (== ',') $ T.pack $ head $ tail input
    print $ solve1 time $ catMaybes ids
    print $ solve2 ids

solve1 :: Integral c => c -> [c] -> c
solve1 t = uncurry (*) . minimumBy (compare `on` snd) . map (\id -> (id, id - t `mod` id))

solve2 :: [Maybe Integer] -> Integer
solve2 xs = f $ catMaybes $ zipWith (\x y -> if isJust y then Just (fromMaybe undefined y - x, fromMaybe undefined y) else Nothing) [0 ..] xs
 where
    f :: [(Integer, Integer)] -> Integer
    f xs =  (`mod` k) $ sum $ map (\(b,m) -> product $ map ($(b,m)) [n,n',c]) xs
     where
        k = product $ map snd xs
        n (_, m) = k `div` m
        n' (b, m) = head [n' | n' <- [1 .. m], (n (b,m) * n') `mod` m == 1]
        c (b, m) = b `mod` m
