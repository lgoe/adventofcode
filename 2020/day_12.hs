{-# LANGUAGE TypeApplications #-}

module Main where

type Dir = (Int, Int)

type Pos = (Int, Int)

type State = (Pos, Dir)

add2 :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
add2 (x, y) (x', y') = (x + x', y + y')

(!*) :: (Num a) => a -> (a, a) -> (a, a)
x !* (y, z) = (x * y, x * z)

main :: IO ()
main = do
  input <- lines <$> readFile "day_12_input.txt"
  print $ solve1 input
  print $ solve2 input

turnLeft :: Dir -> Dir
turnLeft (x, y) = (- y, x)

turnRight :: Dir -> Dir
turnRight (x, y) = (y, - x)

manhattan :: Pos -> Int
manhattan p = sum $ map (abs . ($ p)) [fst, snd]

turnAround :: Dir -> Dir
turnAround = ((-1) !*)

doStep1 :: State -> String -> State
doStep1 (p, d) s = case head s of
  'N' -> (add2 p $ val !* (0, 1), d)
  'S' -> (add2 p $ val !* (0, -1), d)
  'E' -> (add2 p $ val !* (1, 0), d)
  'W' -> (add2 p $ val !* (-1, 0), d)
  'L' -> case val of
    90  -> (p, turnLeft d)
    180 -> (p, turnAround d)
    270 -> (p, turnRight d)
  'R' -> case val of
    90  -> (p, turnRight d)
    180 -> (p, turnAround d)
    270 -> (p, turnLeft d)
  'F' -> (add2 p $ val !* d, d)
  where
    val = (read @Int) $ tail s

solve1 :: [String] -> Int
solve1 = manhattan . fst . foldl doStep1 ((0, 0), (1, 0))

doStep2 :: (Pos, Pos) -> String -> (Pos, Pos)
doStep2 (p, w) s = case head s of
  'N' -> (p, add2 w $ val !* (0, 1))
  'S' -> (p, add2 w $ val !* (0, -1))
  'E' -> (p, add2 w $ val !* (1, 0))
  'W' -> (p, add2 w $ val !* (-1, 0))
  'L' -> case val of
    90  -> (p, turnLeft w)
    180 -> (p, turnAround w)
    270 -> (p, turnRight w)
  'R' -> case val of
    90  -> (p, turnRight w)
    180 -> (p, turnAround w)
    270 -> (p, turnLeft w)
  'F' -> (add2 p $ val !* w, w)
  where
    val = (read @Int) $ tail s

solve2 :: [String] -> Int
solve2 = manhattan . fst . foldl doStep2 ((0, 0), (10, 1))
