{-# LANGUAGE TypeApplications #-}

module Main where

import Data.IntMap.Lazy (IntMap)
import qualified Data.IntMap.Lazy as M
import qualified Data.Text as T

main = do
  input <- readFile "day_15_input.txt"
  let start = map (read @Int . T.unpack) $ T.split (== ',') $ T.pack input
  -- return $ map (\x -> getNth (M.fromList [(0, 1), (3, 2), (6, 3)]) x (6) 3) [4 .. 2020]
  print $ solve 2020 start
  print $ solve 30000 start
  print $ solve 300000 start
  print $ solve 3000000 start
  print $ solve 30000000 start

solve :: Int -> [Int] -> Int
solve n xs = getNth (M.fromList $ zip xs [1 ..]) n (last xs) (length xs)

getNth :: IntMap Int -> Int -> Int -> Int -> Int
getNth m n last pos
  | n == pos = last
  | otherwise = getNth m' n this (pos + 1)
 where
  m' = M.insert last pos m
  this = case M.lookup last m of
    Nothing -> 0
    Just i -> pos - i

-- numbers :: [(Int, Int)] -> [(Int, Int)]
-- numbers xs = xs ++ map (f $ numbers xs) (tail $ tail $ numbers xs)
--   where
--     f :: [(Int, Int)] -> (Int, Int) -> (Int, Int)
--     f xs (i, y) =
--         ( i + 1
--         , case reverse $ filter ((== y) . snd) xs' of
--             [] -> 0
--             ((j, _) : _) -> i - j
--         )
--       where
--         xs' = takeWhile ((< i) . fst) xs
