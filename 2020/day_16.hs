module Main where

import Data.Char (isDigit)
import Data.List (elemIndex, sortOn, (\\))
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M
import Data.Maybe (fromMaybe)
import Text.ParserCombinators.ReadP

type Interval = (Int, Int)
type Range = [Interval]
type Ticket = [Int]

isBetween :: Ord a => a -> (a, a) -> Bool
isBetween x (y, y') = y <= x && x <= y'

isContained :: Ord a => a -> [(a, a)] -> Bool
isContained = any . isBetween

isSingleton :: [a] -> Bool
isSingleton [_] = True
isSingleton _ = False

mapSnd :: (b -> c) -> (a, b) -> (a, c)
mapSnd f (x, y) = (x, f y)

intervalReader :: ReadP Interval
intervalReader = do
    x <- read <$> munch1 isDigit
    char '-'
    y <- read <$> munch1 isDigit
    return (x, y)

-- >>> readP_to_S rangeReader "seat: 13-40 or 45-50"
-- [([(13,40)]," or 45-50"),([(13,40),(45,50)],"")]

rangeReader :: ReadP Range
rangeReader = do
    munch1 (/= ':')
    string ": "
    sepBy1 intervalReader (string " or ")

-- >>> readP_to_S ticketReader "7,3,47"
-- [([7],",3,47"),([7,3],",47"),([7,3,47],"")]

ticketReader :: ReadP Ticket
ticketReader = map read <$> sepBy1 (munch1 isDigit) (char ',')

inputReader :: ReadP ([Range], Ticket, [Ticket])
inputReader = do
    rs <- sepBy1 rangeReader (char '\n')
    skipSpaces
    string "your ticket:"
    skipSpaces
    my <- ticketReader
    skipSpaces
    string "nearby tickets:"
    skipSpaces
    ts <- sepBy1 ticketReader (char '\n')
    skipSpaces
    eof
    return (rs, my, ts)

readInput :: FilePath -> IO ([Range], Ticket, [Ticket])
readInput path = fst . head . readP_to_S inputReader <$> readFile path

main :: IO ()
main = do
    (rs, my, ts) <- readInput "day_16_input.txt"
    print $ solve1 rs ts
    print $ solve2 rs my $ filter (allValid rs) ts

-- >>> allValid [[(1,3),(5,7)],[(6,11),(33,44)],[(13,40),(45,50)]] [7,3,47]
-- True
-- >>> allValid [[(1,3),(5,7)],[(6,11),(33,44)],[(13,40),(45,50)]] [40,4,50]
-- False

isInvalid :: [Range] -> Int -> Bool
isInvalid rs x = not . any (isContained x) $ rs

allValid :: [Range] -> Ticket -> Bool
allValid rs = not . any (isInvalid rs)

solve1 :: [Range] -> [Ticket] -> Int
solve1 rs = sum . filter (isInvalid rs) . concat

solve2 :: [Range] -> Ticket -> [Ticket] -> Int
solve2 rs my ts = product $ map ((my !!) . fst) $ take 6 $ sortOn snd $ map (mapSnd (\r -> fromMaybe (-1) $ elemIndex r rs)) $ deconflict $ M.toList $ foldl (f rs) initP ts
  where
    initP :: Map Int [Range]
    initP = M.fromList [(x, rs) | x <- [0 .. length ts -1]]

    f :: [Range] -> Map Int [Range] -> Ticket -> Map Int [Range]
    f rs m t = foldr (\i -> M.adjust (filter $ isContained $ t !! i) i) m [0 .. length rs - 1]

deconflict :: [(Int, [Range])] -> [(Int, Range)]
deconflict xss
    | all (isSingleton . snd) xss = map (mapSnd head) xss
    | otherwise = deconflict $ map (\(i, xs) -> if isSingleton xs then (i, xs) else (i, xs \\ fixed)) xss
  where
    fixed = map (head . snd) $ filter (isSingleton . snd) xss
