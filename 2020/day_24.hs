module Main where

import Data.List
import Data.Set (Set)
import qualified Data.Set as Set

data Direction = E | SE | SW | W | NW | NE deriving (Show, Eq)

dirs :: [Direction]
dirs = [E, SE, SW, W, NW, NE]

type Pos = (Int, Int)

add2 :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
add2 (x, y) (x', y') = (x + x', y + y')

dir2pos :: Direction -> Pos
dir2pos E = (1, 0)
dir2pos SE = (1, -1)
dir2pos SW = (0, -1)
dir2pos W = (-1, 0)
dir2pos NW = (-1, 1)
dir2pos NE = (0, 1)

-- >>> parse "wenwwweseeeweswwwnwwe"
-- [W,E,NW,W,W,E,SE,E,E,W,E,SW,W,W,NW,W,E]

parse :: String -> [Direction]
parse [] = []
parse (x : xs) = case x of
    'e' -> E : parse xs
    'w' -> W : parse xs
    _ ->
        let y : ys = xs
         in case [x, y] of
                "se" -> SE : parse ys
                "sw" -> SW : parse ys
                "nw" -> NW : parse ys
                "ne" -> NE : parse ys

black1 :: [String] -> Set Pos
black1 = Set.fromList . foldl f [] . map (foldl (\p d -> add2 p (dir2pos d)) (0, 0) . parse)
  where
    f xs y
        | y `elem` xs = delete y xs
        | otherwise = y : xs

solve1 :: [String] -> Int
solve1 = Set.size . black1

shouldBlack :: Set Pos -> Pos -> Bool
shouldBlack ps p
    | p `Set.member` ps = blackNs `elem` [1, 2]
    | otherwise = blackNs == 2
  where
    blackNs = length $ filter (`Set.member` ps) $ map (add2 p . dir2pos) dirs

nextDay :: Set Pos -> Set Pos
nextDay ps = Set.fromList [(x, y) | x <- [- nmax .. nmax], y <- [- nmax .. nmax], shouldBlack ps (x, y)]
  where
    nmax = 1 + maximum (map (abs . fst) (Set.toList ps) ++ map (abs . snd) (Set.toList ps))

solve2 :: [String] -> Int
solve2 = Set.size . (!! 100) . iterate nextDay . black1

main = do
    input <- lines <$> readFile "day_24_input.txt"
    print $ solve1 input
    print $ solve2 input
