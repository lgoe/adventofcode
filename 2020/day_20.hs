{-# LANGUAGE LambdaCase #-}

module Main where

import Data.Char (isDigit)
import Data.Function (on)
import Data.List
import Text.ParserCombinators.ReadP

data Pixel = White | Black deriving (Show, Eq)

type Image = [[Pixel]]

type Tile = (Int, Image)
type TileID = (Int, [Int])

dim :: Int
dim = 10

add2 :: Num a => (a, a) -> (a, a) -> (a, a)
add2 (x, y) (x', y') = (x + x', y + y')

isValid :: Tile -> Bool
isValid (_, t) = length t == dim && all ((== dim) . length) t

-- >>> readP_to_S pixelP "."
-- [(Black,"")]
-- >>> readP_to_S pixelP "#"
-- [(White,"")]

pixelP :: ReadP Pixel
pixelP = (White <$ char '#') +++ (Black <$ char '.')

-- >>>  readP_to_S tileP "Tile 2311:\n..##.#..#.\n##..#.....\n#...##..#.\n####.#...#\n##.##.###.\n##...#.###\n.#.#.#..##\n..#....#..\n###...#.#.\n..###..###\n"
-- [((2311,[[White,White,Black,Black,White,Black,White,White,Black,White],[Black,Black,White,White,Black,White,White,White,White,White],[Black,White,White,White,Black,Black,White,White,Black,White],[Black,Black,Black,Black,White,Black,White,White,White,Black],[Black,Black,White,Black,Black,White,Black,Black,Black,White],[Black,Black,White,White,White,Black,White,Black,Black,Black],[White,Black,White,Black,White,Black,White,White,Black,Black],[White,White,Black,White,White,White,White,Black,White,White],[Black,Black,Black,White,White,White,Black,White,Black,White],[White,White,Black,Black,Black,White,White,Black,Black,Black]]),"")]

tileP :: ReadP Tile
tileP = do
  string "Tile"
  skipSpaces
  id <- read <$> munch1 isDigit
  char ':'
  skipSpaces
  ps <- count dim $ count dim pixelP <* char '\n'
  return (id, ps)

tilesP :: ReadP [Tile]
tilesP = sepBy1 tileP (char '\n') <* eof

parseIntput :: FilePath -> IO [Tile]
parseIntput path = filter isValid . maybe [] fst . find (null . snd) . readP_to_S tilesP <$> readFile path

-- >>> borderToInt [Black,Black,Black,White,Black,Black,White,Black,Black,White]
-- 73

borderToInt :: [Pixel] -> Int
borderToInt = foldl1 (\x y -> 2 * x + y) . map (\case Black -> 0; White -> 1)

convertTile :: Tile -> TileID
convertTile (id, ps) =
  ( id
  , map
      borderToInt
      [ head ps --up
      , reverse $ head ps
      , map last ps --right
      , reverse $ map last ps
      , last ps --down
      , reverse $ last ps
      , map head ps --left
      , reverse $ map head ps
      ]
  )

flipT :: Tile -> Tile
flipT (id, ps) = (id, reverse ps)

rotate90T :: Tile -> Tile
rotate90T (id, ps) = (id, transpose $ reverse ps)

rearrangeT :: Tile -> [Tile]
rearrangeT ps =
  map
    ($ps)
    [ id
    , rotate90T
    , rotate90T . rotate90T
    , rotate90T . rotate90T . rotate90T
    , flipT
    , rotate90T . flipT
    , rotate90T . rotate90T . flipT
    , rotate90T . rotate90T . rotate90T . flipT
    ]

innerBorders :: [Tile] -> [Int]
innerBorders = map head . filter ((> 1) . length) . group . sort . concatMap (snd . convertTile)

isCorner :: [Tile] -> Tile -> Bool
isCorner ts = (4 ==) . length . (`intersect` innerBorders ts) . snd . convertTile

placeNext :: ([[Tile]], [Tile]) -> ([[Tile]], [Tile])
placeNext (im, []) = (im, [])
placeNext ([], ts) = ([[tile']], delete tile ts)
 where
  tile = head $ filter (isCorner ts) ts
  tile' = head $ filter (f . convertTile) $ rearrangeT tile
  f :: TileID -> Bool
  f (_, bs) = all (`notElem` innerBorders ts) [bs !! 2, bs !! 4]
placeNext ([] : xs : im, ts) = case tile of
  Just y -> ([y] : xs : im, deleteBy ((==) `on` fst) y ts)
 where
  tile = find (f . convertTile) $ concatMap rearrangeT ts
  f :: TileID -> Bool
  f (_, bs) = bs !! 4 == snd (convertTile $ last xs) !! 0
placeNext ((x : xs) : im, ts) = case tile of
  Just y -> ((y : x : xs) : im, deleteBy ((==) `on` fst) y ts)
  Nothing -> ([] : (x : xs) : im, ts)
 where
  tile = find (f . convertTile) $ concatMap rearrangeT ts
  f :: TileID -> Bool
  f (_, bs) = bs !! 2 == snd (convertTile x) !! 6

lfp :: Eq a => (a -> a) -> a -> a
lfp f x = fst $ head $ filter (uncurry (==)) $ zip (iterate f x) (tail $ iterate f x)

createImage :: [Tile] -> Image
createImage ts = concatMap (foldl1 unionH . map removeBorder) $ fst $ lfp placeNext ([], ts)
 where
  removeBorder :: Tile -> Image
  removeBorder (_, ps) = map (init . tail) $ init $ tail ps

  unionH :: Image -> Image -> Image
  unionH = zipWith (++)

flipI :: Image -> Image
flipI = reverse

rotate90I :: Image -> Image
rotate90I = transpose . reverse

rearrangeI :: Image -> [Image]
rearrangeI im =
  map
    ($im)
    [ id
    , rotate90I
    , rotate90I . rotate90I
    , rotate90I . rotate90I . rotate90I
    , flipI
    , rotate90I . flipI
    , rotate90I . rotate90I . flipI
    , rotate90I . rotate90I . rotate90I . flipI
    ]

seaMonster :: [(Int, Int)]
seaMonster = [(0, 1), (1, 2), (4, 2), (5, 1), (6, 1), (7, 2), (10, 2), (11, 1), (12, 1), (13, 2), (16, 2), (17, 1), (18, 0), (18, 1), (19, 1)]

hasMonsterAt :: Image -> (Int, Int) -> Bool
hasMonsterAt im c = all ((\(x, y) -> y < length im && x < length (head im) && White == (im !! y) !! x) . add2 c) seaMonster

monsterSpots :: Image -> [(Int, Int)]
monsterSpots im = filter (hasMonsterAt im) [(x, y) | y <- [0 .. length im -1], x <- [0 .. length (head im) -1]]

applyMonster :: [(Int, Int)] -> (Int, Int) -> [(Int, Int)]
applyMonster list c = list \\ map (add2 c) seaMonster

solve1 :: [Tile] -> Int
solve1 ts = product $ map fst $ filter (isCorner ts) ts

solve2 :: [Tile] -> Int
solve2 ts = minimum $ map (\im -> length $ foldl applyMonster (list im) $ monsterSpots im) $ rearrangeI $createImage ts
 where
  list im = [(x, y) | y <- [0 .. length im -1], x <- [0 .. length (head im) -1], White == (im !! y) !! x]

main :: IO ()
main = do
  ts <- parseIntput "day_20_input.txt"
  print $ solve1 ts
  print $ solve2 ts
