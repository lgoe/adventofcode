{-# LANGUAGE LambdaCase #-}

module Main where

import           Data.List (sort, (\\))

main :: IO ()
main = do
    ps <- lines <$> readFile "day_5_input.txt"
    print $ solve1 ps
    print $ solve2 ps

binToDec :: [Int] -> Int
binToDec = foldl (\x y -> 2 * x + y) 0

seatId :: [Char] -> Int
seatId =
    binToDec
        . map (
            \case
                'F' -> 0
                'B' -> 1
                'L' -> 0
                'R' -> 1
                _   -> -1
            )

solve1 :: [[Char]] -> Int
solve1 = maximum . map seatId

solve2 :: [[Char]] -> Int
solve2 ps = head $ [minimum ids .. maximum ids] \\ ids
  where
    ids = sort $ map seatId ps
