module Main where

import qualified Data.Map.Strict as M
import Data.Maybe (mapMaybe)

data Square = Floor | Empty | Occupied deriving (Show, Eq)
type Coords = (Int, Int)
type Grid = M.Map Coords Square

isEmpty :: Square -> Bool
isEmpty Empty = True
isEmpty _ = False

isOccupied :: Square -> Bool
isOccupied Occupied = True
isOccupied _ = False

atMaybe :: [a] -> Int -> Maybe a
atMaybe xs i
  | i < 0 = Nothing
  | otherwise = f i xs
 where
  f :: Int -> [a] -> Maybe a
  f _ [] = Nothing
  f 0 (x : _) = Just x
  f i (_ : xs) = f (i -1) xs

add2 :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
{-# INLINE add2 #-}
add2 (x, y) (x', y') = (x + x', y + y')

-- >>> main
-- ProgressCancelledException

main = do
  input <- readFile "day_11_input.txt"
  let seats = map (map parse) $ lines input
  let grid = M.fromList $ concatMap (\(j, xs) -> zipWith (\i x -> ((i, j), x)) [0 ..] xs) $ zip [0 ..] seats
  print $ solve1 grid
  print $ solve2 grid

parse :: Char -> Square
parse '.' = Floor
parse 'L' = Empty
parse '#' = Occupied

neighbors :: [(Int, Int)]
neighbors = [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]

countOccupiedNeighbors :: Grid -> Coords -> Int
countOccupiedNeighbors g c = length $ filter isOccupied $ mapMaybe ((g M.!?) . add2 c) neighbors

countOccupiedSeenNeighbors :: Grid -> Coords -> Int
countOccupiedSeenNeighbors g c = length $ filter isOccupied $ mapMaybe (seeNeighbor g c) neighbors

seeNeighbor :: Grid -> Coords -> (Int, Int) -> Maybe Square
seeNeighbor g c d = case g M.!? c' of
  Just Floor -> seeNeighbor g c' d
  other -> other
 where
  c' = add2 c d

doRound :: (Grid -> Coords -> Int) -> Int -> Grid -> Grid
doRound f n g =
  M.fromList
    [ (c, s') | (c, s) <- M.toAscList g, let s'
                                              | isEmpty s && f g c == 0 = Occupied
                                              | isOccupied s && f g c >= n = Empty
                                              | otherwise = s
    ]

lfp :: Eq a => (a -> a) -> a -> a
lfp f x = fst $ head $ filter (uncurry (==)) $ zip xs (tail xs)
 where
  xs = iterate f x

solve1 :: Grid -> Int
solve1 = length . filter isOccupied . map snd . M.toAscList . lfp (doRound countOccupiedNeighbors 4)

solve2 :: Grid -> Int
solve2 = length . filter isOccupied . map snd . M.toAscList . lfp (doRound countOccupiedSeenNeighbors 5)
