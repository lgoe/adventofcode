module Main where

import Control.Monad (void)
import Data.Char (isDigit, isSpace)
import Data.List (sortOn)
import Text.ParserCombinators.ReadP

type Index = Int

data Rule
    = CharRule Char
    | SplitRule [Index]
    | AnyRule [Rule]
    deriving (Show)

-- >>> readRule "1: \"a\""
-- (1,CharRule 'a')
-- >>> readRule "0: 1 2"
-- (0,SplitRule [1,2])
-- >>> readRule "11: 42 31 | 42 11 31"
-- (11,AnyRule [SplitRule [42,31],SplitRule [42,11,31]])
-- >>> readRule "99: 72 | 35"
-- (99,AnyRule [SplitRule [72],SplitRule [35]])

readRule :: String -> (Int, Rule)
readRule s =
    ( read $ takeWhile isDigit s
    , rule
    )
  where
    rulePart = drop 2 $ dropWhile isDigit s
    rule
        | head rulePart == '"' = CharRule $ rulePart !! 1
        | '|' `elem` rulePart =
            AnyRule
                [ readSplit $ takeWhile (/= '|') rulePart
                , readSplit $ tail $ dropWhile (/= '|') rulePart
                ]
        | otherwise = readSplit rulePart
    readSplit :: String -> Rule
    readSplit s
        | null s'' = SplitRule [read $ takeWhile isDigit s']
        | null s''' = SplitRule [read $ takeWhile isDigit s', read $ takeWhile isDigit s'']
        | otherwise = SplitRule [read $ takeWhile isDigit s', read $ takeWhile isDigit s'', read $ takeWhile isDigit s''']
      where
        s' = dropWhile isSpace s
        s'' = dropWhile isSpace $ dropWhile isDigit s'
        s''' = dropWhile isSpace $ dropWhile isDigit s''

messageParser :: [Rule] -> Rule -> ReadP ()
messageParser _ (CharRule c) = void $ char c
messageParser grs (SplitRule is) = mapM_ (messageParser grs . (grs !!)) is
messageParser grs (AnyRule rs) = choice $ map (messageParser grs) rs

isMatched :: [Rule] -> String -> Bool
isMatched grs = not . null . readP_to_S (messageParser grs (head grs) >> eof)

solve1 :: [Rule] -> [String] -> Int
solve1 grs = length . filter (isMatched grs)

solve2 :: [Rule] -> [String] -> Int
solve2 grs = length . filter (isMatched grs')
  where
    grs' =
        take 8 grs
            ++ [ AnyRule [SplitRule [42], SplitRule [42, 8]]
               , grs !! 9
               , grs !! 10
               , AnyRule [SplitRule [42, 31], SplitRule [42, 11, 31]]
               ]
            ++ drop 12 grs

main :: IO ()
main = do
    input <- lines <$> readFile "day_19_input.txt"
    let (rs, _ : ms) = break null input
    let grs = map snd $ sortOn fst $ map readRule rs
    print $ solve1 grs ms
    print $ solve2 grs ms
