{-# LANGUAGE LambdaCase #-}

module Main where

import Data.Char (isDigit)
import Data.List (find)
import Data.Maybe (fromMaybe)
import Text.ParserCombinators.ReadP

data MathAST a = ASTValue a | ASTSum (MathAST a) (MathAST a) | ASTProd (MathAST a) (MathAST a) deriving (Show, Eq)

eval :: (Num a) => MathAST a -> a
eval (ASTValue x) = x
eval (ASTSum x y) = eval x + eval y
eval (ASTProd x y) = eval x * eval y

-- >>> readP_to_S numberP "42"
-- [(42,"")]

numberP :: (Num a, Read a) => ReadP a
numberP = read <$> munch1 isDigit

innerFormulaP :: (Num a, Read a) => ReadP (MathAST a) -> ReadP (MathAST a)
innerFormulaP innerP = (ASTValue <$> numberP) +++ (char '(' *> innerP <* char ')')

expressionP :: (Num a, Read a) => ReadP (MathAST a)
expressionP = chainl1 (innerFormulaP expressionP) $ (ASTSum <$ string " + ") +++ (ASTProd <$ string " * ")

sumP' :: (Num a, Read a) => ReadP (MathAST a)
sumP' = chainl1 (innerFormulaP expressionP') (ASTSum <$ string " + ")

productP' :: (Num a, Read a) => ReadP (MathAST a)
productP' = chainl1 sumP' (ASTProd <$ string " * ")

expressionP' :: (Num a, Read a) => ReadP (MathAST a)
expressionP' = productP'

parseExpression :: (Num a, Read a) => ReadP (MathAST a) -> String -> Maybe (MathAST a)
parseExpression p = (fst <$>) . find ((== "") . snd) . readP_to_S p

solve :: ReadP (MathAST Int) -> [String] -> Int
solve p = sum . fromMaybe [] . mapM ((eval <$>) . parseExpression p)

solve1 :: [String] -> Int
solve1 = solve expressionP

solve2 :: [String] -> Int
solve2 = solve expressionP'

main :: IO ()
main = do
    input <- lines <$> readFile "day_18_input.txt"
    print $ solve1 input
    print $ solve2 input
