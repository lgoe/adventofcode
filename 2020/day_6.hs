module Main where

import           Data.List (groupBy, intersect, nub, union)

main = do
    input <- readInput "day_6_input.txt"
    print $ solve1 input
    print $ solve2 input

readInput :: FilePath -> IO [String]
readInput path = map unlines . groupBy (const (/= "")) . lines <$> readFile path

solve1 :: [String] -> Int
solve1 = sum . map (length . foldl1 union . map nub . filter (not . null) . lines)

solve2 :: [String] -> Int
solve2 = sum . map (length . foldl1 intersect . map nub . filter (not . null) . lines)
