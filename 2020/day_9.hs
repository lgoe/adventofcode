module Main where

import           Data.List (inits, tails)

main :: IO ()
main = do
    xs <- map read . lines <$> readFile "day_9_input.txt"
    let x = solve1 25 xs
    print x
    print $ solve2 x xs

fitsRule :: (Eq a, Num a) => [a] -> a -> Bool
fitsRule xs = (`elem` [a + b | a <- xs, b <- xs, a /= b])

sublists :: Eq a => [a] -> [[a]]
sublists = concatMap inits . tails

minMax :: Ord a => [a] -> [a]
minMax xs = [minimum xs, maximum xs]

solve1 :: (Eq a, Num a) => Int -> [a] -> a
solve1 n xs = xs !! head (filter (\i -> not $ fitsRule (take n $ drop (i - n) xs) (xs !! i)) [n .. length xs -1])

solve2 :: (Ord a, Num a) => a -> [a] -> a
solve2 x = sum . minMax . head . filter ((x ==) . sum). filter ((>= 2) . length) . sublists . filter (<x)
