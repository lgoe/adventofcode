module Main where

main :: IO ()
main = do
    input <- map read . lines <$> readFile "day_1_input.txt"
    print $ solve1 2020 input
    print $ solve2 2020 input

solve1 :: Int -> [Int] -> Int
solve1 n xs = uncurry (*) $ head $ filter ((== n) . uncurry (+)) $ [(x, y) | x <- xs, y <- xs]

solve2 :: Int -> [Int] -> Int
solve2 n xs = product $ head $ filter ((== n) . sum) $ [[x, y, z] | x <- xs, y <- xs, z <- xs]
