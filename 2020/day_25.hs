module Main where

import Data.List (elemIndex)
import Data.Maybe (fromMaybe)

g :: Int
g = 7

applyNTimes :: Int -> (a -> a) -> a -> a
applyNTimes 0 _ x = x
applyNTimes n f x = applyNTimes (n -1) f (f x)

step :: Int -> Int -> Int
step sub = (`mod` 20201227) . (* sub)

transform :: Int -> Int -> Int
transform loop sub = applyNTimes loop (step sub) 1

bruteForceLoopSize :: Int -> Int -> Int
bruteForceLoopSize res sub = fromMaybe (-1) $ res `elemIndex` iterate (step sub) 1

-- bruteForceLoopSize res sub = length $ unfoldr next 1
--   where
--     next x
--         | x == res = Nothing
--         | otherwise = Just (step sub x, step sub x)

main = do
    (pub_door : pub_card : _) <- map read . lines <$> readFile "day_25_input.txt"
    let priv_door = bruteForceLoopSize pub_door g
    let priv_card = bruteForceLoopSize pub_card g

    let key1 = transform priv_card pub_door
    let key2 = transform priv_door pub_card
    print $ key1 == key2
    let key = key1
    print key
