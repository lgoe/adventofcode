{-# LANGUAGE LambdaCase #-}

module Main where

import Data.Char
import Data.List
import Data.Maybe

type Cup = Int

type Pos = Int

-- >>> nextOrder  [1..4]
-- [2,3,4,1]

applyNTimes :: Int -> (a -> a) -> (a -> a)
applyNTimes n f = foldr (.) id (replicate n f)

orders :: [a] -> [[a]]
orders xs = [drop n xs ++ take n xs | n <- [0 .. length xs -1]]

fixOrder :: Eq a => a -> [a] -> Maybe [a]
fixOrder x = find ((x ==) . head) . orders

nextOrder :: [a] -> [a]
nextOrder xs = tail xs ++ [head xs]

move1 :: [Cup] -> [Cup]
move1 (c : c1 : c2 : c3 : cs) = nextOrder $ c : partA ++ [c1, c2, c3] ++ partB
  where
    dest =
      if any (< c) cs
        then maximum $ filter (< c) cs
        else maximum cs
    destIndex = (1 +) $ fromMaybe (-1) $ elemIndex dest cs
    (partA, partB) = splitAt destIndex cs

solve1 :: [Cup] -> [Cup]
solve1 = tail . fromMaybe [] . fixOrder 1 . applyNTimes 100 move1

move2 :: ([Pos], Int) -> ([Pos], Int)
move2 (ps, c) =
  (ps'', 1 + mod (fromMaybe (-1) $ elemIndex (1 + ps'' !! (c -1)) ps'') (length ps))
  where
    (ps', i) = if (ps !! (c -1)) < length ps - 4 then (ps, ps !! (c -1)) else (map ((`mod` length ps) . (+ 5)) ps, (`mod` length ps) . (+ 5) $ ps !! (c -1))
    moved = map ((1 +) . fromMaybe (-1) . (`elemIndex` ps') . (`mod` length ps')) [i + 1, i + 2, i + 3]
    dest
      | c >= 2 && notElem (c -1) moved = c -1
      | c >= 3 && notElem (c -2) moved = c -2
      | c >= 4 && notElem (c -3) moved = c -3
      | c >= 5 && notElem (c -4) moved = c -4
      | otherwise = maximum $ [1 .. length ps'] \\ moved
    destIndex = ps' !! (dest -1)
    ps'' =
      if i < destIndex
        then
          map
            ( \case
                j
                  | j <= i -> j
                  | j <= i + 3 -> j - i + destIndex - 3
                  | j <= destIndex -> j -3
                  | otherwise -> j
            )
            ps'
        else
          map
            ( \case
                j
                  | j <= destIndex -> j
                  | j <= i -> j + 3
                  | j <= i + 3 -> j - i + destIndex
                  | otherwise -> j
            )
            ps'

solve2 :: [Cup] -> Int
solve2 cs = firstVal * secondVal
  where
    cupsToPos :: [Cup] -> [Pos]
    cupsToPos cs = take 9 $ map (fromMaybe (-1) . flip elemIndex cs) [1 .. length cs] ++ [length cs ..]

    lastPos = fst $ applyNTimes 2 move2 (cupsToPos cs, head cs)
    firstVal = (1 +) $ fromMaybe (-1) $ elemIndex (1 + head lastPos) lastPos
    secondVal = (1 +) $ fromMaybe (-1) $ elemIndex (2 + head lastPos) lastPos

pos2ord :: [Pos] -> [Cup]
pos2ord ps = [(1 +) $fromMaybe (-1) $ elemIndex i ps | i <- [0 .. length ps -1]]

main = do
  input <- readFile "day_23_input.txt"
  print $ concatMap show $ solve1 $ map digitToInt $ head $ lines input
  print $ solve2 $ map digitToInt $ head $ lines input
