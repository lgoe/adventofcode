Advent of Code
==============

Solutions by me.

| Year | Current state  |
|------|---|
| 2015 | N/A  |
| 2016 | N/A  |
| 2017 | some tasks solved in Dec 2021, using julia  |
| 2018 | some tasks solved in Dec 2018, using python  |
| 2019 | some tasks solved, code is lost |
| 2020 | mostly finished in Jan 2021, using Haskell |
| 2021 | finished in Dec 2021, using julia  |
