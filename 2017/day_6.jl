using Test

test_input = [0,2,7,0]
input = [2,8,8,5,4,2,3,1,5,5,1,2,15,13,5,14]

function part1(bank::Vector{Int})
    bank = copy(bank)
    history = Vector{Int}[]
    steps = 0
    while !(bank in history)
        push!(history, copy(bank))
        i = findfirst(x -> x == maximum(bank), bank)
        left = bank[i]
        bank[i] = 0
        i = (i % length(bank)) + 1
        while left > 0
            bank[i] += 1
            left -= 1
            i = (i % length(bank)) + 1
        end
        steps += 1
    end
    return steps
end

@test part1(test_input) == 5
@show part1(input)


function part2(bank::Vector{Int})
    bank = copy(bank)
    history = Dict{Vector{Int}, Int}()
    step = 0
    while !(bank in keys(history))
        push!(history, copy(bank) => step)
        i = findfirst(x -> x == maximum(bank), bank)
        left = bank[i]
        bank[i] = 0
        i = (i % length(bank)) + 1
        while left > 0
            bank[i] += 1
            left -= 1
            i = (i % length(bank)) + 1
        end
        step += 1
    end
    return step - history[bank]
end

@test part2(test_input) == 4
@show part2(input)
