using Test

include("day_4_input.jl")

function part1_isvalid(phrase)
    words = split(phrase)
    return length(unique(words)) == length(words)
end

function part1(data)
    lines = split(data, '\n')
    return count(part1_isvalid, lines)
end

@test part1_isvalid("aa bb cc dd ee") == true
@test part1_isvalid("aa bb cc dd aa") == false
@test part1_isvalid("aa bb cc dd aaa") == true
@show part1(input)


function part2_isvalid(phrase)
    words = split(phrase)
    char_words = map(collect, words)
    map(sort!, char_words)
    return length(unique(char_words)) == length(char_words)
end

function part2(data)
    lines = split(data, '\n')
    return count(part2_isvalid, lines)
end

@test part2_isvalid("abcde fghij") == true
@test part2_isvalid("abcde xyz ecdab") == false
@test part2_isvalid("a ab abc abd abf abj") == true
@test part2_isvalid("iiii oiii ooii oooi oooo") == true
@test part2_isvalid("oiii ioii iioi iiio") == false
@show part2(input)
