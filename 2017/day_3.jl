using Test

input = 325489

function part1(x)
    r(k) = k + (2k-1)^2
    t(k) = r(k) + 2k
    l(k) = r(k) + 4k
    b(k) = r(k) + 6k
    dist(x, k) = minimum(k + abs(f(k) - x) for f in [r,t,l,b])
    max_k = ceil(Int, sqrt(x)/2)
    return minimum(dist(x, k) for k in 0:max_k)
end

@test part1(1) == 0
@test part1(12) == 3
@test part1(23) == 2
@test part1(1024) == 31
@show part1(input)


function part2(x)
    res = HTTP.get("http://oeis.org/A141481/b141481.txt")
    for line in split(String(res.body), "\n")
        if (isempty(line) || line[1] == '#')
            continue
        end
        y = parse(Int, split(line)[2])
        if y > x
            return y
        end
        println(line)
    end
end

@show part2(input)
