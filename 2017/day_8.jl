using Test

include("day_8_input.jl")

function process_line(regs, line)
    reg, op, num, _, cond_reg, cond_op, cond_num = split(line)
    if cond_op == ">" && !(get(regs, cond_reg, 0) > parse(Int, cond_num))
        return
    elseif cond_op == "<" && !(get(regs, cond_reg, 0) < parse(Int, cond_num))
        return
    elseif cond_op == ">=" && !(get(regs, cond_reg, 0) >= parse(Int, cond_num))
        return
    elseif cond_op == "<=" && !(get(regs, cond_reg, 0) <= parse(Int, cond_num))
        return
    elseif cond_op == "==" && !(get(regs, cond_reg, 0) == parse(Int, cond_num))
        return
    elseif cond_op == "!=" && !(get(regs, cond_reg, 0) != parse(Int, cond_num))
        return
    end
    if op == "inc"
        regs[reg] = get(regs, reg, 0) + parse(Int, num)
    else
        regs[reg] = get(regs, reg, 0) - parse(Int, num)
    end
end

function part1(input)
    regs = Dict{String, Int}()

    for line in split(input, '\n')
        process_line(regs, line)
    end
    return maximum(values(regs))
end

@test part1(test_input) == 1
@show part1(input)


function part2(input)
    regs = Dict{String, Int}()
    alltimemax = 0
    for line in split(input, '\n')
        process_line(regs, line)
        alltimemax = max(alltimemax, maximum(values(regs); init=0))
    end
    return alltimemax
end

@test part2(test_input) == 10
@show part2(input)
