using Test

include("day_5_input.jl")


function part1(offsets::Vector{Int})
    offsets = copy(offsets)
    steps = 0
    pc = 1
    while 0 < pc <= length(offsets)
        offsets[pc] += 1
        pc += offsets[pc] - 1
        steps += 1
        # println(offsets, ' ', pc)
    end
    return steps
end

@test part1([0,3,0,1,-3]) == 5
@show part1(input)


function part2(offsets::Vector{Int})
    offsets = copy(offsets)
    steps = 0
    pc = 1
    while 0 < pc <= length(offsets)
        if offsets[pc] >= 3
            offsets[pc] -= 1
            pc += offsets[pc] + 1
        else
            offsets[pc] += 1
            pc += offsets[pc] - 1
        end
        steps += 1
        # println(offsets, ' ', pc)
    end
    return steps
end

@test part2([0,3,0,1,-3]) == 10
@show part2(input)
