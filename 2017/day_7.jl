using Test

include("day_7_input.jl")


function parse_input(input)
    weights = Dict{String, Int}()
    children = Dict{String, Vector{String}}()

    for line in split(input, '\n')
        splits = split(line)
        if length(splits) == 2
            name, weight = splits
            rest = []
        else
            name, weight, _, rest... = splits
            rest = map(s -> replace(s, "," => ""), rest)
        end
        weight = weight[2:end-1]

        weights[name] = parse(Int, weight)
        children[name] = rest
    end
    return weights, children
end

function find_root(children)
    cands = Set(keys(children))
    for cs in values(children)
        for c in cs
            cands = delete!(cands, c)
        end
    end
    return pop!(cands)
end

function part1(input)
    weights, children = parse_input(input)
    return find_root(children)
end

@test part1(test_input) == "tknk"
@show part1(input)


function get_cum_weights(weights, root, children)
    if isempty(children[root])
        return Dict{String, Int}(root => weights[root]), weights[root]
    else
        c_weights = map(c -> get_cum_weights(weights, c, children), children[root])
        w = weights[root] + sum(x -> x[2], c_weights)
        return merge(Dict{String, Int}(root => w), map(x -> x[1], c_weights)...), w
    end
end

function find_error(weights, cum_weights, root, children)
    cands = Set(keys(children))
    filter!(c -> length(children[c]) > 0, cands)
    filter!(c -> length(unique([cum_weights[s] for s in children[c]])) >= 2, cands)
    error = ""
    for c in cands
        if isempty(intersect(cands, children[c]))
            error = c
            break
        end
    end
    s_weights = [cum_weights[s] for s in children[error]]
    if (s_weights[1] == s_weights[2])
        ref = s_weights[1]
        i = findfirst(x -> x != ref, s_weights)
    elseif (s_weights[1] == s_weights[3])
        ref = s_weights[1]
        i = 2
    else
        ref = s_weights[2]
        i = 1
    end
    return ref - cum_weights[children[error][i]] + weights[children[error][i]]
end

function part2(input)
    weights, children = parse_input(input)
    root = find_root(children)
    cum_weights = get_cum_weights(weights, root, children)[1]
    return find_error(weights, cum_weights, root, children)
end

@test part2(test_input) == 60
@show part2(input)
