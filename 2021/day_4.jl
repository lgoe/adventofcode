using Test

include("day_4_input.jl")

function parse_input(input)
    parts = split(input, "\n\n")
    order = map(x -> parse(Int, string(x)), split(parts[1], ','))
    boards = map(b -> map(l -> map(n -> parse(Int, n), split(l)), split(b, '\n')), parts[2:end])
    return order, boards
end

function mark_num!(board, num)
    map(line -> replace!(line, num => -1), board)
    return board
end

function wins(board)
    board = map(line -> map(x -> x+1, line), board)
    return any(line -> sum(line) == 0, board) || any(col -> col == 0, sum(board))
end

function part1(input)
    order, boards = parse_input(input)
    i=0
    while !any(wins, boards)
        i += 1
        num = order[i]
        boards = map(b -> mark_num!(b, num), boards)
    end

    win_board = filter(wins, boards)[1]
    return order[i] * sum(sum(map(x -> max(0, x), line)) for line in win_board)
end

@test part1(test_input) == 24 * 188
@show part1(input)


function part2(input)
    order, boards = parse_input(input)
    i=0
    while !all(wins, boards)
        filter!(!wins, boards)
        i += 1
        num = order[i]
        boards = map(b -> mark_num!(b, num), boards)
    end

    win_board = boards[1]
    return order[i] * sum(sum(map(x -> max(0, x), line)) for line in win_board)
end

@test part2(test_input) == 13 * 148
@show part2(input)
