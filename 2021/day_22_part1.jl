using Test

include("day_22_input.jl")

struct Cuboid{T}
    xmin  :: Int
    xmax  :: Int
    ymin  :: Int
    ymax  :: Int
    zmin  :: Int
    zmax :: Int
    value :: T
end

function parse_line(s::AbstractString)
    first, second = split(s)
    turnon = first == "on"
    regex = r"([-0-9]+)"
    xmin, xmax, ymin, ymax, zmin, zmax = [parse(Int64, t.match) for t in eachmatch(regex, second)]
    return Cuboid{Bool}(xmin, xmax, ymin, ymax, zmin, zmax, turnon)
end

function parse_input(input::AbstractString)
    return map(parse_line, split(input, '\n'))
end

function set!(cubes::Array{T, 3}, x::Int, y::Int, z::Int, value::T) where T
    cubes[x+51,y+51,z+51] = value
end

function apply!(cubes::Array{Bool, 3}, ins::Cuboid{Bool})
    for x in max(-50,ins.xmin):min(50,ins.xmax)
        for y in max(-50,ins.ymin):min(50,ins.ymax)
            for z in max(-50,ins.zmin):min(50,ins.zmax)
                set!(cubes, x, y, z, ins.value)
            end
        end
    end
end

function part1(input)
    inss = parse_input(input)
    cubes = fill(false, 151, 151, 151)
    for ins in inss
        apply!(cubes, ins)
    end
    return count(cubes)
end

@test part1(test_input1) == 39
@test part1(test_input2) == 590784
@test part1(test_input3) == 474140
@show part1(input)
