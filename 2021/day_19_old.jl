using Test
using Random

include("day_19_input.jl")

function parse_input(input)
    beaconsPerScanner = Vector{Tuple{Int,Int,Int}}[]
    parts = split(input, "\n\n")
    for part in parts
        beacons = Tuple{Int,Int,Int}[]
        for line in split(part, '\n')[2:end]
            coords = split(line, ',')
            x, y, z = map(s -> parse(Int, s), coords)
            push!(beacons, (x,y,z))
        end
        push!(beaconsPerScanner, beacons)
    end
    return beaconsPerScanner
end

function orientations()
    os = Function[]
    push!(os, (x,y,z) -> (x,y,z))
    push!(os, (x,y,z) -> (x,-y,-z))
    push!(os, (x,y,z) -> (-x,-y,z))
    push!(os, (x,y,z) -> (-x,y,-z))
    push!(os, (x,y,z) -> (x,z,-y))
    push!(os, (x,y,z) -> (x,-z,y))
    push!(os, (x,y,z) -> (-x,z,y))
    push!(os, (x,y,z) -> (-x,-z,-y))
    push!(os, (x,y,z) -> (y,z,x))
    push!(os, (x,y,z) -> (y,-z,-x))
    push!(os, (x,y,z) -> (-y,-z,x))
    push!(os, (x,y,z) -> (-y,z,-x))
    push!(os, (x,y,z) -> (y,x,-z))
    push!(os, (x,y,z) -> (y,-x,z))
    push!(os, (x,y,z) -> (-y,x,z))
    push!(os, (x,y,z) -> (-y,-x,-z))
    push!(os, (x,y,z) -> (z,x,y))
    push!(os, (x,y,z) -> (z,-x,-y))
    push!(os, (x,y,z) -> (-z,-x,y))
    push!(os, (x,y,z) -> (-z,x,-y))
    push!(os, (x,y,z) -> (z,y,-x))
    push!(os, (x,y,z) -> (z,-y,x))
    push!(os, (x,y,z) -> (-z,y,x))
    push!(os, (x,y,z) -> (-z,-y,-x))
    return os
end

function Base.:+(p1::Tuple{Int64, Int64, Int64}, p2::Tuple{Int64, Int64, Int64})
    return (p1[1] + p2[1], p1[2] + p2[2], p1[3] + p2[3])
end

function Base.:-(p1::Tuple{Int64, Int64, Int64}, p2::Tuple{Int64, Int64, Int64})
    return (p1[1] - p2[1], p1[2] - p2[2], p1[3] - p2[3])
end

function dist(p1::Tuple{Int64, Int64, Int64}, p2::Tuple{Int64, Int64, Int64})
    return abs(p1[1] - p2[1]) +  abs(p1[2] - p2[2]) + abs(p1[3] - p2[3])
end

function accumulate_beacons!(beacons::Vector{Tuple{Int,Int,Int}}, scanners::Vector{Tuple{Int,Int,Int}}, beaconsPerScanner::Vector{Vector{Tuple{Int,Int,Int}}})
    for (i,scanner) in enumerate(beaconsPerScanner)
        for o in orientations()
            ps = map(p -> o(p...), scanner)
            for b in beacons
                refp = Random.rand(ps)
                diff = b - refp
                tryps = map(p -> p + diff, ps)
                if length(intersect(beacons, tryps)) >= 12
                    append!(beacons, tryps)
                    unique!(beacons)
                    deleteat!(beaconsPerScanner, i)
                    push!(scanners, diff)
                    return
                end
            end
        end
    end
end

function solution(input)
    beaconsPerScanner = parse_input(input)
    beacons = popat!(beaconsPerScanner, 1)
    scanners = Tuple{Int,Int,Int}[]
    while !isempty(beaconsPerScanner)
        @info "progress" scannerleft=length(beaconsPerScanner) merged=length(beacons)
        accumulate_beacons!(beacons, scanners, beaconsPerScanner)
    end
    return length(beacons), maximum(pair -> dist(pair...), (s1, s2) for s1 in scanners for s2 in scanners)
end

@test solution(test_input) == (79, 3621)
@show solution(input)
