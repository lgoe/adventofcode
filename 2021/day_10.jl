using Test

include("day_10_input.jl")

function is_opening(c::Char)
    return in(c, ['(','[','{','<'])
end

function matching_closing_char(c::Char)
    if c == '('
        return ')'
    elseif c == '['
        return ']'
    elseif c == '{'
        return '}'
    elseif c == '<'
        return '>'
    else
        return '\0'
    end
end

function find_first_wrong(line)
    to_close = Vector{Char}()
    for c in line
        if is_opening(c)
            push!(to_close, c)
        else
            if c != matching_closing_char(pop!(to_close))
                return c
            end
        end
    end
    return '\0'
end

function char_to_points1(c::Char)
    if c == ')'
        return 3
    elseif c == ']'
        return 57
    elseif c == '}'
        return 1197
    elseif c == '>'
        return 25137
    else
        return 0
    end
end

function part1(input)
    return sum(line -> char_to_points1(find_first_wrong(line)), split(input, '\n'))
end

@test part1(test_input) == 26397
@show part1(input)

function isincomplete(line)
    return find_first_wrong(line) == '\0'
end

function find_missing(line)
    to_close = Vector{Char}()
    for c in line
        if is_opening(c)
            push!(to_close, c)
        else
            pop!(to_close)
        end
    end
    return reverse(map(matching_closing_char, to_close))
end

function char_to_points2(c::Char)
    if c == ')'
        return 1
    elseif c == ']'
        return 2
    elseif c == '}'
        return 3
    elseif c == '>'
        return 4
    else
        return 0
    end
end

function get_score2(cs)
    score = 0
    for c in cs
        score *= 5
        score += char_to_points2(c)
    end
    return score
end

function part2(input)
    scores = [get_score2(find_missing(line)) for line in split(input, '\n') if isincomplete(line)]
    sort!(scores)
    return scores[div(length(scores)+1, 2)]
end

@test part2(test_input) == 288957
@show part2(input)
