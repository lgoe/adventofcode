using Test

include("day_13_input.jl")

function part1(input)
    input_points, input_folds = split(input, "\n\n")
    points = Set{Tuple{Int,Int}}()
    for line in split(input_points, '\n')
        x, y = map(s -> parse(Int, s), split(line, ','))
        push!(points, (x,y))
    end

    for line in split(input_folds, '\n')[1:1]
        dir = line[12]
        num = parse(Int, split(line, '=')[2])
        if dir == 'x'
            points = Set(map(collect(points)) do (x,y)
                if x > num
                    x = 2*num - x
                end
                return (x,y)
            end)
        else
            points = Set(map(collect(points)) do (x,y)
                if y > num
                    y = 2*num - y
                end
                return (x,y)
            end)
        end
    end
    return length(points)
end

@test part1(test_input) == 17
@show part1(input)


function part2(input)
    input_points, input_folds = split(input, "\n\n")
    points = Set{Tuple{Int,Int}}()
    for line in split(input_points, '\n')
        x, y = map(s -> parse(Int, s), split(line, ','))
        push!(points, (x,y))
    end

    for line in split(input_folds, '\n')
        dir = line[12]
        num = parse(Int, split(line, '=')[2])
        if dir == 'x'
            points = Set(map(collect(points)) do (x,y)
                if x > num
                    x = 2*num - x
                end
                return (x,y)
            end)
        else
            points = Set(map(collect(points)) do (x,y)
                if y > num
                    y = 2*num - y
                end
                return (x,y)
            end)
        end
    end
    minx, maxx = extrema([x for (x,y) in points])
    miny, maxy = extrema([y for (x,y) in points])
    m = fill('.' ^ (maxx-minx+1), maxy-miny+1)
    for (x,y) in points
        m[y-miny+1] = m[y-miny+1][1:x-minx] * '#' * m[y-miny+1][x-minx+2:end]
    end
    return m
end

@show part2(test_input)
@show part2(input)
