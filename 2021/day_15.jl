using Test

using Graphs, SimpleWeightedGraphs

include("day_15_input.jl")

function parse_graph(input)
    lines = split(input, '\n')
    height = length(lines)
    width = length(lines[1])
    sources_down = [x + (y-1)*width for y in 1:height-1 for x in 1:width]
    dests_down = [x + (y-1)*width for y in 2:height for x in 1:width]
    weights_down = [lines[y][x]-'0' for y in 2:height for x in 1:width]

    sources_up = [x + (y-1)*width for y in 2:height for x in 1:width]
    dests_up = [x + (y-1)*width for y in 1:height-1 for x in 1:width]
    weights_up = [lines[y][x]-'0' for y in 1:height-1 for x in 1:width]

    sources_right = [x + (y-1)*width for y in 1:height for x in 1:width-1]
    dests_right = [x + (y-1)*width for y in 1:height for x in 2:width]
    weights_right = [lines[y][x]-'0' for y in 1:height for x in 2:width]

    sources_left = [x + (y-1)*width for y in 1:height for x in 2:width]
    dests_left = [x + (y-1)*width for y in 1:height for x in 1:width-1]
    weights_left = [lines[y][x]-'0' for y in 1:height for x in 1:width-1]

    sources = [sources_down; sources_up; sources_right; sources_left]
    dests = [dests_down; dests_up; dests_right; dests_left]
    weights = [weights_down; weights_up; weights_right; weights_left]

    return SimpleWeightedDiGraph(sources, dests, weights), width, height
end

function part1(input)
    g, width, height = parse_graph(input)
    return dijkstra_shortest_paths(g, 1).dists[width*height]
end

@test part1(test_input) == 40
@show part1(input)


function part2(input)
    g, width, height = parse_graph(modify_input(input))
    return dijkstra_shortest_paths(g, 1).dists[width*height]
end

function modify_input(input)
    lines = split(input, '\n')
    lines = [join(map(c -> c+i+j, line) for j in 0:4) for i in 0:4 for line in lines]
    data = join(lines, '\n')
    data = map(function (c)
            if c == '\n'
                return c
            else
                return '1' + (c - '1') % 9
            end
        end, data)
    return data
end

@test part2(test_input) == 315
@show part2(input)
