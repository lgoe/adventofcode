using Test
using Combinatorics

include("day_8_input.jl")

function part1(data)
    num_unique = 0
    for line in split(data, '\n')
        left, right = split(line, " | ")
        outputs = split(right)
        num_unique += count(output -> in(length(output), [2, 3, 4, 7]), outputs)
    end
    return num_unique
end

@test part1(test_input) == 26
@show part1(input)


function ismultisetequal(l1, l2)
    return sort(l1) == sort(l2)
end

function Base.sort(s::String)
    return join(sort(collect(s)), "")
end

function segs2num(s::String)
    if s == "abcefg"
        return 0
    elseif s == "cf"
        return 1
    elseif s == "acdeg"
        return 2
    elseif s == "acdfg"
        return 3
    elseif s == "bcdf"
        return 4
    elseif s == "abdfg"
        return 5
    elseif s == "abdefg"
        return 6
    elseif s == "acf"
        return 7
    elseif s == "abcdefg"
        return 8
    elseif s == "abcdfg"
        return 9
    else
        throw(ArgumentError("invalid segments"))
    end
end

function deduce(line)
    possible = collect(Combinatorics.permutations(['a','b','c','d','e','f','g']))
    left, right = split(line, " | ")
    digits = split(left)
    sort!(digits; by=length)
    # use 1
    possible = filter(perm -> ismultisetequal(['c','f'], [perm[digits[1][1]-'a'+1],perm[digits[1][2]-'a'+1]]), possible)
    # use 7
    possible = filter(perm -> ismultisetequal(['a','c','f'], [perm[digits[2][1]-'a'+1],perm[digits[2][2]-'a'+1],perm[digits[2][3]-'a'+1]]), possible)
    # use 4
    possible = filter(perm -> ismultisetequal(['b','c','d','f'], [perm[digits[3][1]-'a'+1],perm[digits[3][2]-'a'+1],perm[digits[3][3]-'a'+1],perm[digits[3][4]-'a'+1]]), possible)
    # use 0, 6 and 9
    possible = filter(perm -> ismultisetequal(['a','b','c','e','f','g','a','b','d','e','f','g','a','b','c','d','f','g'], [perm[digits[k][i]-'a'+1] for k in [7,8,9] for i in 1:length(digits[k])]), possible)
    
    if length(possible) != 1
        return possible
    end
    perm = first(possible)

    outputs = map(out -> segs2num(sort(map(c -> perm[c-'a'+1], out))), split(right))
    return sum(10^(i-1) * n for (i,n) in enumerate(reverse(outputs)))
end

@test deduce("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf") == 5353
@test deduce("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe") == 8394
@test deduce("edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc") == 9781
@test deduce("fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg") == 1197
@test deduce("fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb") == 9361
@test deduce("aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea") == 4873
@test deduce("fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb") == 8418
@test deduce("dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe") == 4548
@test deduce("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef") == 1625
@test deduce("egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb") == 8717
@test deduce("gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce") == 4315

function part2(data)
    return sum(deduce, split(data, '\n'))
end

@test part2(test_input) == 61229
@show part2(input)
