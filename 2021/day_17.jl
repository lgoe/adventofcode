using Test

function possible_xs(xmin, xmax, maxs)
    @assert 0 < xmin < xmax
    steps = Dict{Int,Vector{Int}}()
    for x in 1:xmax
        save_x = x
        s = 0
        cumx = 0
        while cumx < xmax && s < maxs
            cumx += x
            x -= sign(x)
            s += 1
            if xmin <= cumx <= xmax
                if !haskey(steps, s)
                    steps[s] = []
                end
                push!(steps[s], save_x)
            end
        end
    end
    return steps
end

function possible_ys(ymin, ymax)
    @assert ymin < ymax < 0
    steps = Dict{Int,Vector{Int}}()
    for y in -abs(ymin):abs(ymin)
        save_y = y
        s = 0
        cumy = 0
        while cumy > ymin
            cumy += y
            y -= 1
            s += 1
            if ymin <= cumy <= ymax
                if !haskey(steps, s)
                    steps[s] = []
                end
                push!(steps[s], save_y)
            end
        end
    end
    return steps
end

function highest_y(y)
    return div(y*(y+1),2)
end

function part1(xmin, xmax, ymin, ymax)
    return maximum(map(highest_y, unique(collect(Iterators.flatten(values(possible_ys(ymin, ymax)))))))
end

@test part1(20, 30, -10, -5) == 45
@show part1(144, 178, -100, -76)


function part2(xmin, xmax, ymin, ymax)
    pos_y = possible_ys(ymin, ymax)
    maxs = maximum(keys(pos_y))
    pos_x = possible_xs(xmin, xmax, maxs)
    return length(unique((x,y) for s in 1:maxs for x in get(pos_x, s, []) for y in get(pos_y, s, [])))
end

@test part2(20, 30, -10, -5) == 112
@show part2(144, 178, -100, -76)


function hits(xmin, xmax, ymin, ymax, x, y)
    cumx = 0
    cumy = 0
    while cumx <= xmax || cumy >= ymin
        cumx += x
        cumy += y
        x -= sign(x)
        y -= 1
        if xmin <= cumx <= xmax && ymin <= cumy <= ymax
            return true
        end
    end
    return false
end

@test hits(20, 30, -10, -5, 7, 2)
@test hits(20, 30, -10, -5, 6, 3)
@test hits(20, 30, -10, -5, 9, 0)
@test !hits(20, 30, -10, -5, 17, -4)
