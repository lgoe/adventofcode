using Test

include("day_3_input.jl")

function part1(data)
    lines = split(data, '\n')
    n = length(lines)
    bits = map(xs -> map(x -> x - '0', xs), map(collect, lines))
    m = length(bits[1])

    gamma = 0
    for (i, s) in enumerate(reverse(sum(bits)))
        if 2*s > n
            gamma += 2^(i-1)
        end
    end
    epsilon = 2^m - gamma - 1
    return gamma * epsilon
end

@test part1(test_input) == 198
@show part1(input)


function oxygen_rating(data)
    lines = split(data, '\n')
    bits = map(xs -> map(x -> x - '0', xs), map(collect, lines))
    n = length(bits)
    m = length(bits[1])
    i = 1
    while length(bits) > 1
        n = length(bits)
        m = length(bits[1])
        s = sum(x[i] for x in bits)
        bit = 2*s >= n ? 1 : 0
        bits = filter(x -> x[i] == bit, bits)
        i += 1
    end
    return sum(b*2^(i-1) for (i,b) in enumerate(reverse(bits[1])))
end

function co2_rating(data)
    lines = split(data, '\n')
    bits = map(xs -> map(x -> x - '0', xs), map(collect, lines))
    n = length(bits)
    m = length(bits[1])
    i = 1
    while length(bits) > 1
        n = length(bits)
        m = length(bits[1])
        s = sum(x[i] for x in bits)
        bit = 2*s >= n ? 0 : 1
        bits = filter(x -> x[i] == bit, bits)
        i += 1
    end
    return sum(b*2^(i-1) for (i,b) in enumerate(reverse(bits[1])))
end

function part2(data)
    return oxygen_rating(data) * co2_rating(data)
end

@test oxygen_rating(test_input) == 23
@test co2_rating(test_input) == 10
@test part2(test_input) == 230
@show part2(input)
