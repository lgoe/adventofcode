using Test

include("day_11_input.jl")

function input_to_matrix(input)
    lines = split(input, '\n')
    m = Matrix{Int}(undef, length(lines), length(lines))
    for (i, line) in enumerate(lines)
        m[i,:] = [c - '0' for c in line]
    end
    return m
end

function Base.get(m::Matrix{Int}, x::Int, y::Int)
    if x < 1 || x > size(m, 1) || y < 1 || y > size(m, 2)
        return -1
    else
        return m[x,y]
    end
end

function inc!(m::Matrix{Int}, x::Int, y::Int)
    if x < 1 || x > size(m, 1) || y < 1 || y > size(m, 2)
        return
    else
        return m[x,y] += 1
    end
end

function doStep!(octs)
    map!(o -> o+1, octs, octs)
    flashes = 0
    while any(o -> o > 9, octs)
        p = findfirst(o -> o > 9, octs)
        flashes += 1
        octs[p] = 0
        for dx in -1:1, dy in -1:1
            if get(octs, p[1]+dx, p[2]+dy) > 0
                inc!(octs, p[1]+dx, p[2]+dy)
            end
        end
    end
    return flashes
end

function part1(input, steps)
    octs = input_to_matrix(input)
    i = 0
    total_flashes = 0
    while i < steps
        total_flashes += doStep!(octs)
        i += 1
    end
    return total_flashes
end

@test part1(test_input, 100) == 1656
@show part1(input, 100)


function part2(input)
    octs = input_to_matrix(input)
    i = 1
    while doStep!(octs) < 100
        i += 1
    end
    return i
end

@test part2(test_input) == 195
@show part2(input)
