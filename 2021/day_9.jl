using Test

include("day_9_input.jl")

function get(lines::Vector{String}, x, y)
    if y < 1 || y > length(lines) || x < 1 || x > length(lines[1])
        return 10
    else
        return lines[y][x] - '0'
    end
end

function get(m::Matrix{T}, x, y) where T
    if y < 1 || y > size(m, 2) || x < 1 || x > size(m, 1)
        return missing
    else
        return m[x,y]
    end
end

function low_points(lines)
    low_points = Vector{Tuple{Int,Int}}()
    for y in 1:length(lines), x in 1:length(lines[y])
        if get(lines, x, y) < minimum([get(lines, x + dx, y + dy) for (dx,dy) in [(1,0),(-1,0),(0,1),(0,-1)]])
            push!(low_points, (x,y))
        end
    end
    return low_points
end

function part1(input)
    lines = map(String, split(input, '\n'))
    low_ps = low_points(lines)
    return sum(p -> 1 + get(lines, p[1], p[2]), low_ps)
end

@test part1(test_input) == 15
@show part1(input)


function part2(input)
    lines = map(String, split(input, '\n'))
    low_ps = low_points(lines)
    basin_assignment = Matrix{Union{Missing, Tuple{Int,Int}}}(missing, length(lines[1]), length(lines))
    for p in low_ps
        basin_assignment[p[1],p[2]] = p
    end
    for _ in 1:8
        for y in 1:length(lines), x in 1:length(lines[y])
            if get(lines, x, y) != 9
                neighbor_basins = skipmissing(get(basin_assignment, x + dx, y + dy) for (dx,dy) in [(1,0),(-1,0),(0,1),(0,-1)])
                if !isempty(neighbor_basins)
                    basin_assignment[x,y] = first(neighbor_basins)
                end
            end
        end
    end
    basin_count = [count(x -> x == p, skipmissing(values(basin_assignment))) for p in low_ps]
    sort!(basin_count)
    return prod(basin_count[end-2:end])
end

@test part2(test_input) == 1134
@show part2(input)
