using Test

include("day_22_input.jl")

struct Cuboid{T}
    xmin  :: Int
    xmax  :: Int
    ymin  :: Int
    ymax  :: Int
    zmin  :: Int
    zmax :: Int
    value :: T
end

function parse_line(s::AbstractString)
    first, second = split(s)
    turnon = first == "on"
    regex = r"([-0-9]+)"
    xmin, xmax, ymin, ymax, zmin, zmax = [parse(Int64, t.match) for t in eachmatch(regex, second)]
    return Cuboid{Bool}(xmin, xmax, ymin, ymax, zmin, zmax, turnon)
end

function parse_input(input::AbstractString)
    return map(parse_line, split(input, '\n'))
end

function intersect(cub1::Cuboid, cub2::Cuboid, value::T) where T
    xmin = max(cub1.xmin, cub2.xmin)
    xmax = min(cub1.xmax, cub2.xmax)
    ymin = max(cub1.ymin, cub2.ymin)
    ymax = min(cub1.ymax, cub2.ymax)
    zmin = max(cub1.zmin, cub2.zmin)
    zmax = min(cub1.zmax, cub2.zmax)
    if xmin <= xmax && ymin <= ymax && zmin <= zmax
        return Cuboid{T}(xmin, xmax, ymin, ymax, zmin, zmax, value)
    else
        return nothing
    end
end

function updatevalue(cub::Cuboid, value::T) where T
    return Cuboid{T}(cub.xmin, cub.xmax, cub.ymin, cub.ymax, cub.zmin, cub.zmax, value)
end

function Base.size(cub::Cuboid)
    return (cub.xmax-cub.xmin+1) * (cub.ymax-cub.ymin+1) * (cub.zmax-cub.zmin+1)
end

function apply!(cuboids::Vector{Cuboid{Int}}, ins::Cuboid{Bool})
    intersections = filter(!isnothing, map(cuboids) do cub
        intersect(cub, ins, -cub.value)
    end)
    if ins.value
        return append!(cuboids, intersections, [updatevalue(ins, 1)])
    else
        return append!(cuboids, intersections)
    end
end

function part1(input)
    inss = filter(!isnothing, map(parse_input(input)) do ins
        intersect(Cuboid(-50, 50, -50, 50, -50, 50, 0), ins, ins.value)
    end)
    cuboids = Cuboid{Int}[]
    for ins in inss
        apply!(cuboids, ins)
    end
    return sum(cuboids) do cub
        size(cub) * cub.value
    end
end

@test part1(test_input1) == 39
@test part1(test_input2) == 590784
@test part1(test_input3) == 474140
@show part1(input)


function part2(input)
    inss = parse_input(input)
    cuboids = Cuboid{Int}[]
    for ins in inss
        apply!(cuboids, ins)
    end
    return sum(cuboids) do cub
        size(cub) * cub.value
    end
end

@test part2(test_input1) == 39
@test part2(test_input3) == 2758514936282235
@show part2(input)
