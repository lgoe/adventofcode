using Test

include("day_16_input.jl")

abstract type Packet end

struct LiteralValuePacket <: Packet
    version :: Int
    value :: Int
end

struct OperatorPacket <: Packet
    version :: Int
    typeID :: Int
    subpackets :: Vector{Packet}
end

function Base.:(==)(p1::LiteralValuePacket, p2::LiteralValuePacket)
    return p1.value == p2.value
end

function Base.:(==)(p1::OperatorPacket, p2::OperatorPacket)
    return p1.typeID == p2.typeID && p1.subpackets == p2.subpackets
end

function hex_to_binary(s)
    return join(map(function(c)
        if c == '0' return "0000"
        elseif c == '1' return "0001"
        elseif c == '2' return "0010"
        elseif c == '3' return "0011"
        elseif c == '4' return "0100"
        elseif c == '5' return "0101"
        elseif c == '6' return "0110"
        elseif c == '7' return "0111"
        elseif c == '8' return "1000"
        elseif c == '9' return "1001"
        elseif c == 'A' return "1010"
        elseif c == 'B' return "1011"
        elseif c == 'C' return "1100"
        elseif c == 'D' return "1101"
        elseif c == 'E' return "1110"
        elseif c == 'F' return "1111"
        end
    end, collect(s)), "")
end

@test hex_to_binary("D2FE28") == "110100101111111000101000"

function binary_to_int(s)
    return sum((v-'0') << (i-1) for (i,v) in enumerate(reverse(s)))
end

function parse_packet(s)
    version = binary_to_int(s[1:3])
    typeID = binary_to_int(s[4:6])
    if typeID == 4
        i = 0
        value_str = ""
        while s[7+5i] == '1'
            value_str *= s[7+5i+1:7+5i+4]
            i += 1
        end
        value_str *= s[7+5i+1:7+5i+4]
        value = binary_to_int(value_str)
        return LiteralValuePacket(version, value), s[7+5i+5:end]
    else
        length_type_ID = s[7]
        subpackets = Vector{Packet}()
        if length_type_ID == '0'
            len_subs = binary_to_int(s[7+1:7+15])
            rest = s[7+15+1:7+15+len_subs]
            while !isempty(rest)
                pack, rest = parse_packet(rest)
                push!(subpackets, pack)
            end
            rest = s[7+15+len_subs+1:end]
        else
            n_subs = binary_to_int(s[7+1:7+11])
            rest = s[7+12:end]
            for _ in 1:n_subs
                pack, rest = parse_packet(rest)
                push!(subpackets, pack)
            end
        end
        return OperatorPacket(version, typeID, subpackets), rest
    end
end

@test parse_packet(hex_to_binary("D2FE28"))[1] == LiteralValuePacket(6, 2021)
@test parse_packet("110100101111111000101000")[1] == LiteralValuePacket(6, 2021)

@test parse_packet(hex_to_binary("38006F45291200"))[1] == 
    OperatorPacket(1, 6, [LiteralValuePacket(-1, 10), LiteralValuePacket(-1, 20)])
@test parse_packet("00111000000000000110111101000101001010010001001000000000")[1] ==
    OperatorPacket(1, 6, [LiteralValuePacket(-1, 10), LiteralValuePacket(-1, 20)])

@test parse_packet(hex_to_binary("EE00D40C823060"))[1] == 
    OperatorPacket(7, 3, [LiteralValuePacket(-1, 1), LiteralValuePacket(-1, 2), LiteralValuePacket(-1, 3)])
@test parse_packet("11101110000000001101010000001100100000100011000001100000")[1] == 
    OperatorPacket(7, 3, [LiteralValuePacket(-1, 1), LiteralValuePacket(-1, 2), LiteralValuePacket(-1, 3)])


function add_versions(p::LiteralValuePacket)
    return p.version
end

function add_versions(p::OperatorPacket)
    return p.version + sum(add_versions, p.subpackets)
end

function part1(input)
    return add_versions(parse_packet(hex_to_binary(input))[1])
end

@test part1("8A004A801A8002F478") == 16
@test part1("620080001611562C8802118E34") == 12
@test part1("C0015000016115A2E0802F182340") == 23
@test part1("A0016C880162017C3686B18A3D4780") == 31
@show part1(input)


function value(p::LiteralValuePacket)
    return p.value
end

function value(p::OperatorPacket)
    if p.typeID == 0
        return sum(value, p.subpackets)
    elseif p.typeID == 1
        return prod(value, p.subpackets)
    elseif p.typeID == 2
        return minimum(value, p.subpackets)
    elseif p.typeID == 3
        return maximum(value, p.subpackets)
    elseif p.typeID == 5
        return value(p.subpackets[1]) > value(p.subpackets[2]) ? 1 : 0
    elseif p.typeID == 6
        return value(p.subpackets[1]) < value(p.subpackets[2]) ? 1 : 0
    elseif p.typeID == 7
        return value(p.subpackets[1]) == value(p.subpackets[2]) ? 1 : 0
    end
    return 0
end

function part2(input)
    return value(parse_packet(hex_to_binary(input))[1])
end

@test part2("C200B40A82") == 3
@test part2("04005AC33890") == 54
@test part2("880086C3E88112") == 7
@test part2("CE00C43D881120") == 9
@test part2("D8005AC2A8F0") == 1
@test part2("F600BC2D8F") == 0
@test part2("9C005AC2F8F0") == 0
@test part2("9C0141080250320F1802104A08") == 1
@show part2(input)
