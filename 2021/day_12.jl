using Test

include("day_12_input.jl")

function parse_graph(s)
    adj = Dict{String, Vector{String}}()

    for line in split(s, '\n')
        left, right = split(line, '-')
        if !haskey(adj, left)
            adj[left] = [right]
        else
            push!(adj[left], right)
        end
        if !haskey(adj, right)
            adj[right] = [left]
        else
            push!(adj[right], left)
        end
    end
    return adj
end

function issmall(cave)
    return cave == lowercase(cave)
end

function dfs1(adj, curr, used, target)
    if curr == target
        return 1
    end
    push!(used, curr)
    res = 0
    for neigh in adj[curr]
        if !issmall(neigh) || !in(neigh, used)
            res += dfs1(adj, neigh, used, target)
        end
    end
    pop!(used)
    return res
end

function part1(input)
    adj = parse_graph(input)
    return dfs1(adj, "start", [], "end")
end

@test part1(test_input1) == 10
@test part1(test_input2) == 19
@test part1(test_input3) == 226
@show part1(input)


function dfs2(adj, curr, used, target)
    if curr == target
        return 1
    end
    push!(used, curr)
    res = 0
    for neigh in adj[curr]
        if !issmall(neigh) || !in(neigh, used)
            res += dfs2(adj, neigh, used, target)
        elseif neigh != "start"
            res += dfs1(adj, neigh, used, target)
        end
    end
    pop!(used)
    return res
end

function part2(input)
    adj = parse_graph(input)
    return dfs2(adj, "start", ["start"], "end")
end

@test part2(test_input1) == 36
@test part2(test_input2) == 103
@test part2(test_input3) == 3509
@show part2(input)
