using Test
using Multisets
using Random

include("day_19_input.jl")

function parse_input(input)
    beaconsPerScanner = Vector{Tuple{Int,Int,Int}}[]
    parts = split(input, "\n\n")
    for part in parts
        beacons = Tuple{Int,Int,Int}[]
        for line in split(part, '\n')[2:end]
            coords = split(line, ',')
            x, y, z = map(s -> parse(Int, s), coords)
            push!(beacons, (x,y,z))
        end
        push!(beaconsPerScanner, beacons)
    end
    return beaconsPerScanner
end

function orientations()
    os = Function[]
    push!(os, (x,y,z) -> (x,y,z))
    push!(os, (x,y,z) -> (x,-y,-z))
    push!(os, (x,y,z) -> (-x,-y,z))
    push!(os, (x,y,z) -> (-x,y,-z))
    push!(os, (x,y,z) -> (x,z,-y))
    push!(os, (x,y,z) -> (x,-z,y))
    push!(os, (x,y,z) -> (-x,z,y))
    push!(os, (x,y,z) -> (-x,-z,-y))
    push!(os, (x,y,z) -> (y,z,x))
    push!(os, (x,y,z) -> (y,-z,-x))
    push!(os, (x,y,z) -> (-y,-z,x))
    push!(os, (x,y,z) -> (-y,z,-x))
    push!(os, (x,y,z) -> (y,x,-z))
    push!(os, (x,y,z) -> (y,-x,z))
    push!(os, (x,y,z) -> (-y,x,z))
    push!(os, (x,y,z) -> (-y,-x,-z))
    push!(os, (x,y,z) -> (z,x,y))
    push!(os, (x,y,z) -> (z,-x,-y))
    push!(os, (x,y,z) -> (-z,-x,y))
    push!(os, (x,y,z) -> (-z,x,-y))
    push!(os, (x,y,z) -> (z,y,-x))
    push!(os, (x,y,z) -> (z,-y,x))
    push!(os, (x,y,z) -> (-z,y,x))
    push!(os, (x,y,z) -> (-z,-y,-x))
    return os
end

function Base.:+(p1::Tuple{Int64, Int64, Int64}, p2::Tuple{Int64, Int64, Int64})
    return (p1[1] + p2[1], p1[2] + p2[2], p1[3] + p2[3])
end

function Base.:-(p1::Tuple{Int64, Int64, Int64}, p2::Tuple{Int64, Int64, Int64})
    return (p1[1] - p2[1], p1[2] - p2[2], p1[3] - p2[3])
end

function dist_manhattan(p1::Tuple{Int64, Int64, Int64}, p2::Tuple{Int64, Int64, Int64})
    return abs(p1[1] - p2[1]) +  abs(p1[2] - p2[2]) + abs(p1[3] - p2[3])
end

function distsq_euclidean(p1::Tuple{Int64, Int64, Int64}, p2::Tuple{Int64, Int64, Int64})
    return (p1[1] - p2[1])^2 +  (p1[2] - p2[2])^2 + (p1[3] - p2[3])^2
end

function accumulate_beacons!(beacons::Vector{Tuple{Int,Int,Int}}, dists::Multiset{Int}, scanners::Vector{Tuple{Int,Int,Int}}, beaconsPerScanner::Vector{Vector{Tuple{Int,Int,Int}}}, distsPerScanner::Vector{Multiset{Int}})
    for (i,scanner) in enumerate(beaconsPerScanner)
        if length(intersect(dists, distsPerScanner[i])) >= 2*binomial(12,2)
            for o in orientations()
                ps = map(p -> o(p...), scanner)
                for b in beacons, refp in Random.randsubseq(ps, 1/(5+2*length(beaconsPerScanner)))
                    diff = b - refp
                    tryps = map(p -> p + diff, ps)
                    if length(intersect(beacons, tryps)) >= 12
                        append!(beacons, tryps)
                        unique!(beacons)
                        push!(scanners, diff)
                        deleteat!(beaconsPerScanner, i)
                        deleteat!(distsPerScanner, i)
                        return
                    end
                end
            end
        end
    end
end

function solution(input)
    beaconsPerScanner = parse_input(input)
    beacons = popat!(beaconsPerScanner, 1)
    distsPerScanner = [Multiset([distsq_euclidean(b1, b2) for b1 in bs for b2 in bs if b1 != b2]) for bs in beaconsPerScanner]
    scanners = Tuple{Int,Int,Int}[]
    while !isempty(beaconsPerScanner)
        dists = Multiset([distsq_euclidean(b1, b2) for b1 in beacons for b2 in beacons if b1 != b2])
        @info "progress" scannerleft=length(beaconsPerScanner) merged=length(beacons)
        accumulate_beacons!(beacons, dists, scanners, beaconsPerScanner, distsPerScanner)
    end
    return length(beacons), maximum(pair -> dist_manhattan(pair...), (s1, s2) for s1 in scanners for s2 in scanners)
end

@test solution(test_input) == (79, 3621)
@show solution(input)
