using Test
using DataStructures

include("day_23_input.jl")

struct Position
    hallway :: Vector{Int}
    rooms :: Tuple{Vector{Int},Vector{Int},Vector{Int},Vector{Int}}
end

function Base.:(==)(p1::Position, p2::Position)
    return (p1.hallway, p1.rooms) == (p2.hallway, p2.rooms)
end

function Base.hash(p::Position, h::UInt)
    return hash(p.hallway, hash(p.rooms, h))
end

end_position1 = Position(zeros(Int, 11), ([1, 1], [2, 2], [3, 3], [4, 4]))
end_position2 = Position(zeros(Int, 11), ([1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3], [4, 4, 4, 4]))

function part2_modify(input)
    lines = split(input, '\n')
    lines = [lines[1:3]; part2_add; lines[4:end]]
    return join(lines, '\n')
end

function parse_input(input)
    lines = split(input, '\n')
    hallway = zeros(Int, 11)
    room1 = [line[4]-'A'+1 for line in lines[3:end-1]]
    room2 = [line[6]-'A'+1 for line in lines[3:end-1]]
    room3 = [line[8]-'A'+1 for line in lines[3:end-1]]
    room4 = [line[10]-'A'+1 for line in lines[3:end-1]]
    return Position(hallway, (room1, room2, room3, room4))
end

function hasnowrong(room::Vector{Int}, i::Int)
    return all(r -> r == 0 || r == i, room)
end

function optimize!(p::Position)
    cost = 0
    # move up/down inside a room
    for j in 1:4
        a1 = p.rooms[j][1]
        if hasnowrong(p.rooms[j], j)
            if a1 != 0
                i = findfirst(!=(0), p.rooms[j][2:end])
                if isnothing(i)
                    i = length(p.rooms[j])
                end
                if i > 1
                    p.rooms[j][i] = a1
                    p.rooms[j][1] = 0
                    cost += abs(i - 1) * 10^(a1-1)
                end
            end
        else
            if a1 == 0
                i = findfirst(!=(0), p.rooms[j])
                p.rooms[j][1] = p.rooms[j][i]
                p.rooms[j][i] = 0
                cost += abs(i - 1) * 10^(p.rooms[j][1]-1)
            end
        end

        
    end

    # move from hallway into goal room
    for j in 1:4
        a1 = p.rooms[j][1]
        door_ind = 2*j+1
        goal_a = j
        if a1 == 0 && hasnowrong(p.rooms[j][2:end], j)
            for i in findall(==(goal_a), p.hallway)
                if i < door_ind
                    if iszero(p.hallway[i+1:door_ind])
                        p.rooms[j][1] = goal_a
                        p.hallway[i] = 0
                        cost += (1 + abs(i - door_ind)) * 10^(goal_a-1)
                        break
                    end
                else
                    if iszero(p.hallway[door_ind:i-1])
                        p.rooms[j][1] = goal_a
                        p.hallway[i] = 0
                        cost += (1 + abs(i - door_ind)) * 10^(goal_a-1)
                        break
                    end
                end
            end
        end
    end

    # move from start room into goal room
    for j in 1:4
        a1 = p.rooms[j][1]
        door_ind_start = 2*j+1
        if a1 != 0 && a1 != j
            k = p.rooms[j][1]
            b1 = p.rooms[k][1]
            door_ind_goal = 2*k+1
            if b1 == 0 && hasnowrong(p.rooms[k][2:end], k)
                if door_ind_start < door_ind_goal
                    if iszero(p.hallway[door_ind_start:door_ind_goal])
                        p.rooms[k][1] = a1
                        p.rooms[j][1] = 0
                        cost += (2 + abs(door_ind_start - door_ind_goal)) * 10^(a1-1)
                        break
                    end
                else
                    if iszero(p.hallway[door_ind_goal:door_ind_start])
                        p.rooms[k][1] = a1
                        p.rooms[j][1] = 0
                        cost += (2 + abs(door_ind_start - door_ind_goal)) * 10^(a1-1)
                        break
                    end
                end
            end
        end
    end
    if cost > 0
        p, morecost = optimize!(p)
        return (p, cost + morecost)
    else
        return (p, cost)
    end
end

function neighbors(p::Position)
    ps = Tuple{Position, Int}[]
    door_inds = [2*j + 1 for j in 1:4]
    for j in 1:4
        a1 = p.rooms[j][1]
        door_ind = 2*j+1
        if a1 != 0 && !hasnowrong(p.rooms[j], j)
            for i in [collect(Iterators.takewhile(i -> p.hallway[i] == 0, door_ind-1:-1:1)); collect(Iterators.takewhile(i -> p.hallway[i] == 0, door_ind+1:11))]
                if !in(i, door_inds)
                    newp = deepcopy(p)
                    newp.hallway[i] = a1
                    newp.rooms[j][1] = 0
                    cost = (1 + abs(i - door_ind)) * 10^(a1-1)
                    optp, optcost = optimize!(newp)
                    push!(ps, (optp, cost + optcost))
                end
            end
        end
    end
    return ps
end

function find_cheapest(start_pos::Position, end_pos::Position)
    dists = Dict{Position, Int}()
    #preds = Dict{Position, Position}()
    todo = PriorityQueue{Position, Int}()
    dists[start_pos] = 0
    #preds[start_pos] = start_pos
    enqueue!(todo, start_pos => 0)
    while !isempty(todo)
        curr_pos = dequeue!(todo)
        if curr_pos == end_pos
            return dists[end_pos]
        end
        for (next_pos, cost) in neighbors(curr_pos)
            if !haskey(dists, next_pos)
                new_cost = get(dists, curr_pos, Inf) + cost
                dists[next_pos] = new_cost
                #preds[next_pos] = curr_pos
                enqueue!(todo, next_pos => new_cost)
            elseif in(next_pos, keys(todo))
                new_cost = get(dists, curr_pos, Inf) + cost
                if new_cost < get(dists, next_pos, Inf)
                    dists[next_pos] = new_cost
                    #preds[next_pos] = curr_pos
                    todo[next_pos] = new_cost
                end
            end
        end
    end
end

@test find_cheapest(parse_input(test_input), end_position1) == 12521
@show find_cheapest(parse_input(input), end_position1)

@test find_cheapest(parse_input(part2_modify(test_input)), end_position2) == 44169
@show find_cheapest(parse_input(part2_modify(input)), end_position2)
