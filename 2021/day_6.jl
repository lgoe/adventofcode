using Test

include("day_6_input.jl")

function part1(fs, n)
    fs = copy(fs)
    for _ in 1:n
        new = count(x -> x == 0, fs)
        replace!(fs, 0 => 7)
        map!(x -> x-1, fs, fs)
        append!(fs, [8 for _ in 1:new])
    end
    return length(fs)
end

@test part1(test_input, 18) == 26
@test part1(test_input, 80) == 5934
@show part1(input, 80)


function part2_dict(fs, n)
    d = Dict{Int, Int}()
    for f in fs
        d[f] = get(d, f, 0) + 1
    end

    for _ in 1:n
        new = pop!(d, 0, 0)
        d = Dict{Int, Int}([k-1 => v for (k,v) in pairs(d)])
        d[6] = get(d, 6, 0) + new
        d[8] = get(d, 8, 0) + new
    end
    return sum(values(d))
end

@test part2_dict(test_input, 18) == 26
@test part2_dict(test_input, 80) == 5934
@test part2_dict(test_input, 256) == 26984457539
@show part2_dict(input, 256)

function part2_vec(fs, n)
    d = zeros(Int, 9)
    for f in fs
        d[f+1] += 1
    end

    for _ in 1:n
        new = d[1]
        d[1:end-1] = d[2:end]
        d[end] = 0
        d[7] += new
        d[9] += new
    end
    return sum(d)
end

@show part2_vec(test_input, 0)
@test part2_vec(test_input, 18) == 26
@test part2_vec(test_input, 80) == 5934
@test part2_vec(test_input, 256) == 26984457539
@show part2_vec(input, 256)
