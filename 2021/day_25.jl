using Test

include("day_25_input.jl")

@enum LocContent empty east south

function Base.isempty(l::LocContent)
    return l == empty
end

function Base.show(io::IO, ::MIME"text/plain", l::LocContent)
    if l == empty
        print(io, ". ")
    elseif l == east
        print(io, ">")
    elseif l == south
        print(io, "v ")
    else
        error()
    end
end

function parse_input(input)
    lines = split(input, '\n')
    region = Matrix{LocContent}(undef, length(lines), length(lines[1]))
    for (i, line) in enumerate(lines)
        for (j, c) in enumerate(line)
            if c == '.'
                region[i,j] = empty
            elseif c == '>'
                region[i,j] = east
            elseif c == 'v'
                region[i,j] = south
            else
                error()
            end
        end
    end
    return region
end

function doStep!(region::Matrix{LocContent}, wantstomove::Matrix{Bool})
    h, w = size(region)
    for i in 1:h
        for j in 1:w
            if @inbounds region[i, j] == east
                @inbounds wantstomove[i, j] = isempty(region[i, j % w + 1])
            else
                @inbounds wantstomove[i, j] = false
            end
        end
    end
    unocce = findall(wantstomove)
    @inbounds region[map(p -> CartesianIndex(p[1], p[2] % w + 1), unocce)] .= east
    @inbounds region[unocce] .= empty

    for j in 1:w
        for i in 1:h
            if @inbounds region[i, j] == south 
                @inbounds wantstomove[i, j] = isempty(region[i % h + 1, j])
            else
                @inbounds wantstomove[i, j] = false
            end
        end
    end
    unoccs = findall(wantstomove)
    @inbounds region[map(p -> CartesianIndex(p[1] % h + 1, p[2]), unoccs)] .= south
    @inbounds region[unoccs] .= empty

    return length(unocce) > 0 || length(unoccs) > 0
end

function part1(input)
    region = parse_input(input)
    wantstomove = fill(false, size(region))
    i = 1
    while doStep!(region, wantstomove)
        i += 1
    end
    return i
end

@test part1(test_input) == 58
@show part1(input)

@show 1+1

# using Test

# include("day_25_input.jl")

# @enum LocContent empty east south

# function Base.isempty(l::LocContent)
#     return l == empty
# end

# function Base.show(io::IO, ::MIME"text/plain", l::LocContent)
#     if l == empty
#         print(io, ". ")
#     elseif l == east
#         print(io, ">")
#     elseif l == south
#         print(io, "v ")
#     else
#         error()
#     end
# end

# function parse_input(input)
#     lines = split(input, '\n')
#     region = Matrix{LocContent}(undef, length(lines), length(lines[1]))
#     for (i, line) in enumerate(lines)
#         for (j, c) in enumerate(line)
#             if c == '.'
#                 region[i,j] = empty
#             elseif c == '>'
#                 region[i,j] = east
#             elseif c == 'v'
#                 region[i,j] = south
#             else
#                 error()
#             end
#         end
#     end
#     return region
# end

# function doStep(region::Matrix{LocContent})
#     region1 = fill(empty, size(region))
#     region2 = fill(empty, size(region))
#     h, w = size(region)
#     for i in 1:h, j in 1:w
#         if region[i, j] == east 
#             if isempty(region[i, j % w + 1])
#                 region1[i, j % w + 1] = east
#             else
#                 region1[i, j] = east
#             end
#         elseif region[i, j] == south
#             region1[i, j] = south
#         end
#     end
#     for i in 1:h, j in 1:w
#         if region1[i, j] == south 
#             if isempty(region1[i % h + 1, j])
#                 region2[i % h + 1, j] = south
#             else
#                 region2[i, j] = south
#             end
#         elseif region1[i, j] == east
#             region2[i, j] = east
#         end
#     end
#     return region2
# end

# function part1(input)
#     region = parse_input(input)
#     region_before = fill(empty, size(region))
#     i = 0
#     while region != region_before
#         region_before = region
#         region = doStep(region)
#         i += 1
#     end
#     return i
# end

# @test part1(test_input) == 58
# @show part1(input)
