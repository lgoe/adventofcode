using Test

include("day_5_input.jl")

function part1(input)
    d = Dict{Tuple{Int,Int}, Int}()
    for line in split(input, '\n')
        p1, p2 = split(line, " -> ")
        x1 = parse(Int, split(p1, ',')[1])
        y1 = parse(Int, split(p1, ',')[2])
        x2 = parse(Int, split(p2, ',')[1])
        y2 = parse(Int, split(p2, ',')[2])

        if x1 == x2
            for yi in min(y1,y2):max(y1,y2)
                d[(x1,yi)] = get(d, (x1,yi), 0) + 1
            end
        elseif y1 == y2
            for xi in min(x1,x2):max(x1,x2)
                d[(xi,y1)] = get(d, (xi,y1), 0) + 1
            end
        end
    end

    return count(n -> n >= 2, values(d))
end

@test part1(test_input) == 5
@show part1(input)


function part2(input)
    d = Dict{Tuple{Int,Int}, Int}()
    for line in split(input, '\n')
        p1, p2 = split(line, " -> ")
        x1 = parse(Int, split(p1, ',')[1])
        y1 = parse(Int, split(p1, ',')[2])
        x2 = parse(Int, split(p2, ',')[1])
        y2 = parse(Int, split(p2, ',')[2])

        if x1 == x2
            for yi in min(y1,y2):max(y1,y2)
                d[(x1,yi)] = get(d, (x1,yi), 0) + 1
            end
        elseif y1 == y2
            for xi in min(x1,x2):max(x1,x2)
                d[(xi,y1)] = get(d, (xi,y1), 0) + 1
            end
        elseif x1 < x2
            if y1 < y2
                for (xi, yi) in zip(x1:x2, y1:y2)
                    d[(xi,yi)] = get(d, (xi,yi), 0) + 1
                end
            else
                for (xi, yi) in zip(x1:x2, y1:-1:y2)
                    d[(xi,yi)] = get(d, (xi,yi), 0) + 1
                end
            end
        else
            if y1 < y2
                for (xi, yi) in zip(x1:-1:x2, y1:y2)
                    d[(xi,yi)] = get(d, (xi,yi), 0) + 1
                end
            else
                for (xi, yi) in zip(x1:-1:x2, y1:-1:y2)
                    d[(xi,yi)] = get(d, (xi,yi), 0) + 1
                end
            end
        end
    end

    return count(n -> n >= 2, values(d))
end

@test part2(test_input) == 12
@show part2(input)
