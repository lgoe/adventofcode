using Test

mutable struct DetDie
    nextNumber :: Int
    timesRolled :: Int
    maxNumber :: Int

    function DetDie(maxNumber)
        return new(1, 0, maxNumber)
    end
end

function roll!(die::DetDie)
    rolled = die.nextNumber
    die.nextNumber = (die.nextNumber % die.maxNumber) + 1
    die.timesRolled += 1
    return rolled
end

function turn!(die::DetDie, space1::Int, score1::Int, space2::Int, score2::Int, isfirstturn::Bool)
    rolls = roll!(die) + roll!(die) + roll!(die)
    if isfirstturn
        space1 = ((space1 + rolls - 1) % 10) + 1
        score1 += space1
    else
        space2 = ((space2 + rolls - 1) % 10) + 1
        score2 += space2
    end
    isfirstturn = !isfirstturn
    return space1, score1, space2, score2, isfirstturn
end

function part1(space1, space2)
    score1 = 0
    score2 = 0
    isfirstturn = true
    die = DetDie(100)
    while score1 < 1000 && score2 < 1000
        space1, score1, space2, score2, isfirstturn = turn!(die, space1, score1, space2, score2, isfirstturn)
    end
    return min(score1, score2) * die.timesRolled
end

@test part1(4,8) == 745 * 993
@show part1(8,4)
