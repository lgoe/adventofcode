using Test

include("day_20_input.jl")

function Base.:+(p1::Tuple{Int,Int}, p2::Tuple{Int,Int})
    return (p1[1] + p2[1], p1[2] + p2[2])
end

function parse_input(input)
    algo_string, image_string = split(input, "\n\n")
    algo = map(
        function(c)
            if c == '.'
                return 0
            elseif c == '#'
                return 1
            else
                error()
            end
        end, collect(algo_string))

    lit = Set{Tuple{Int,Int}}()
    for (i, line) in enumerate(split(image_string, '\n'))
        for (j, c) in enumerate(line)
            if c == '#'
                push!(lit, (i,j))
            end
        end
    end
    return algo, lit
end

function enhance(lit, algo, inf_lit=false)
    new_lit = Set{Tuple{Int,Int}}()
    mini = minimum(p -> p[1], lit)
    maxi = maximum(p -> p[1], lit)
    minj = minimum(p -> p[2], lit)
    maxj = maximum(p -> p[2], lit)
    poss = [(i,j) for i in mini-1:maxi+1 for j in minj-1:maxj+1]
    for pos in poss
        bits = map(
            function(dpos)
                p = pos + dpos
                return Int((inf_lit && !(mini <= p[1] <= maxi && minj <= p[2] <= maxj)) || (mini <= p[1] <= maxi && minj <= p[2] <= maxj && in(pos + dpos, lit)))
            end, [(-1,-1),(-1,0),(-1,1),(0,-1),(0,0),(0,1),(1,-1),(1,0),(1,1)])
        bin = sum(v*2^(i-1) for (i,v) in enumerate(reverse(bits)))
        if algo[bin+1] == 1
            push!(new_lit, pos)
        end
    end

    return new_lit
end

function solve(input, n)
    algo, lit = parse_input(input)
    inf_lit = false
    for i in 1:n
        lit = enhance(lit, algo, inf_lit)
        if !inf_lit && algo[1] == 1
            inf_lit = true
        elseif inf_lit && algo[512] == 0
            inf_lit = false
        end
        @info "Progress: $(div(100*i,n))%"
    end
    return length(lit)
end

@test solve(test_input, 2) == 35
@show solve(input, 2)

@test solve(test_input, 50) == 3351
@show solve(input, 50)
