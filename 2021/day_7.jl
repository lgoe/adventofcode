using Test

include("day_7_input.jl")

function dist1(k, c::Int)
    return abs(c-k)
end

function dist1(k, cs::Vector{Int})
    return sum(c -> dist1(k,c), cs)
end

function part1(cs)
    return minimum(dist1(k,cs) for k in minimum(cs):maximum(cs))
end

@test dist1(1, test_input) == 41
@test dist1(2, test_input) == 37
@test dist1(3, test_input) == 39
@test dist1(10, test_input) == 71
@test part1(test_input) == 37
@show part1(input)


function dist2(k, c::Int)
    d = abs(c-k)
    return div(d*(d+1),2)
end

function dist2(k, cs::Vector{Int})
    return sum(c -> dist2(k,c), cs)
end

function part2(cs)
    return minimum(dist2(k,cs) for k in minimum(cs):maximum(cs))
end

@test dist2(2, test_input) == 206
@test dist2(5, test_input) == 168
@test part2(test_input) == 168
@show part2(input)
