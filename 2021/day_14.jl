using Test

include("day_14_input.jl")

function parse_input(input)
    first, second = split(input, "\n\n")
    rules = Dict{Tuple{Char,Char}, Char}(
        begin 
            l, r = map(collect, split(line, " -> "))
            (l[1], l[2]) => r[1]
        end for line in split(second, '\n')
    )
    template = first
    return template, rules
end

function interleave(a::AbstractVector{T}, b::AbstractVector{T}) where T
    if length(b) == length(a) - 1
        x = pop!(a)
        return push!(collect(Iterators.flatten(zip(a,b))), x)
    else
        return collect(Iterators.flatten(zip(a,b)))
    end
end

function doStep(polymer, rules)
    news = [rules[(l1, l2)] for (l1, l2) in zip(polymer, polymer[2:end])]
    return interleave(collect(polymer), news)
end

function doSteps(input, n)
    polymer, rules = parse_input(input)
    for _ in 1:n
        polymer = doStep(polymer, rules)
    end
    return polymer
end

@test join(doSteps(test_input, 1), "") == "NCNBCHB"
@test join(doSteps(test_input, 2), "") == "NBCCNBBBCBHCB"
@test join(doSteps(test_input, 3), "") == "NBBBCNCCNBBNBNBBCHBHHBCHB"
@test join(doSteps(test_input, 4), "") == "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"

function part1(input)
    polymer = doSteps(input, 10)
    counts = [count(c -> c == x, polymer) for x in unique(polymer)]
    return maximum(counts) - minimum(counts)
end

@test part1(test_input) == 1588
@show part1(input)

function part2(input, i_max)
    polymer, rules = parse_input(input)
    chars = unique(collect(Iterators.flatten(keys(rules))))
    dp = Dict{Tuple{Char,Char,Int,Char}, Int}()
    for c1 in chars, c2 in chars, c in chars
        dp[(c1, c2, 0, c)] = 0
    end
    for i in 1:i_max
        for c1 in chars, c2 in chars
            new = rules[(c1, c2)]
            for c in chars
                if c == new
                    dp[(c1, c2, i, c)] = dp[(c1, new, i-1, c)] + 1 + dp[(new, c2, i-1, c)]
                else
                    dp[(c1, c2, i, c)] = dp[(c1, new, i-1, c)] + dp[(new, c2, i-1, c)]
                end
            end
        end
    end
    counts = [count(x -> x == c, polymer) + sum(dp[(c1, c2, i_max, c)] for (c1, c2) in zip(polymer, polymer[2:end])) for c in chars]
    return maximum(counts) - minimum(counts)
end

@test part2(test_input, 10) == 1588
@test part2(test_input, 40) == 2188189693529
@show part2(input, 40)
