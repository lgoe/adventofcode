using Test

maxi = 10
maxj = 21

stepsPerTurn = [0, 0, 1, 3, 6, 7, 6, 3, 1] # there are stepsPerTurn[i] ways to get i points in one turn
@assert sum(stepsPerTurn) == 3^3

M = zeros(Int, 10, maxi+1, maxj+1+length(stepsPerTurn), 10) # M[a,i,j,k] = number of ways to get in i-1 turns to exactly j-1 points and be on space k, assuming one started in space a

M[4,1,1,4] = 1
M[8,1,1,8] = 1
for a in [4,8]
    for i in 1:maxi
        for j in 1:maxj, k in 1:10
            for (l,w) in enumerate(stepsPerTurn)
                nextk = (k + l - 1) % 10 + 1
                M[a, i+1, j+nextk, nextk] += w * M[a, i, j, k]
            end
        end
    end
end

W = sum(M[:,:,maxj+1:end,:], dims=[3,4])  # W[a,i] = number of ways to reach >=21 points in exactly i turns, assuming started on space a

@test (test_num1wins = sum(s1 * sum(M[8,i-1,1:21,:]) for (i,s1) in enumerate(W[4,:]) if i > 1)) == 444356092776315
@test (test_num2wins = sum(s1 * sum(M[4,i,1:21,:]) for (i,s1) in enumerate(W[8,:]))) == 341960390180808

@show num1wins = sum(s1 * sum(M[4,i-1,1:21,:]) for (i,s1) in enumerate(W[8,:]) if i > 1)
@show num2wins = sum(s1 * sum(M[8,i,1:21,:]) for (i,s1) in enumerate(W[4,:]))
@show max(num1wins, num2wins)
