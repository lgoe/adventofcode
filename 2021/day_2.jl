using Test

include("day_2_input.jl")

function part1(data)
    pos_h = 0
    depth = 0
    for (dir, n_str) in map(split, split(data, '\n'))
        n = parse(Int, n_str)
        if dir == "forward"
            pos_h += n
        elseif dir == "up"
            depth -= n
        elseif dir == "down"
            depth += n
        end
    end
    return pos_h * depth
end

@test part1(test_input) == 150
@show part1(input)


function part1(data)
    pos_h = 0
    depth = 0
    aim = 0
    for (dir, n_str) in map(split, split(data, '\n'))
        n = parse(Int, n_str)
        if dir == "forward"
            pos_h += n
            depth += aim * n
        elseif dir == "up"
            aim -= n
        elseif dir == "down"
            aim += n
        end
    end
    return pos_h * depth
end

@test part1(test_input) == 900
@show part1(input)
