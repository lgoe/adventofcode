using Test

include("day_18_input.jl")

mutable struct SnailNumber
    left :: Union{Int, SnailNumber}
    right :: Union{Int, SnailNumber}
end

function Base.:(==)(sn1::SnailNumber, sn2::SnailNumber)
    return (sn1.left, sn1.right) == (sn2.left, sn2.right)
end

function parse_snailnumber(s::AbstractString)
    @assert s[1] == '['
    if s[2] == '['
        left, s = parse_snailnumber(s[2:end])
    else
        left = parse(Int, s[2])
        s = s[3:end]
    end
    @assert s[1] == ','
    if s[2] == '['
        right, s = parse_snailnumber(s[2:end])
    else
        right = parse(Int, s[2])
        s = s[3:end]
    end
    @assert s[1] == ']'
    return SnailNumber(left, right), s[2:end]
end

function Base.show(io::IO, sn::SnailNumber)
    print(io, '[', sn.left, ',', sn.right, ']')
end

function Base.:+(sn1::SnailNumber, sn2::SnailNumber)
    return reduce!(SnailNumber(sn1, sn2))
end

function addtofirst!(sn::SnailNumber, n::Int)
    if isa(sn.left, Int)
        sn.left += n
    else
        addtofirst!(sn.left, n)
    end
end

function addtolast!(sn::SnailNumber, n::Int)
    if isa(sn.right, Int)
        sn.right += n
    else
        addtolast!(sn.right, n)
    end
end

function finddeep(sn::SnailNumber, depth::Int, hist::Vector{SnailNumber} = SnailNumber[])
    push!(hist, sn)
    if depth == 0
        return hist
    end
    if !isa(sn.left, Int)
        temp = finddeep(sn.left, depth-1, hist)
        if !isnothing(temp)
            return temp
        end
    end
    if !isa(sn.right, Int)
        temp = finddeep(sn.right, depth-1, hist)
        if !isnothing(temp)
            return temp
        end
    end
    pop!(hist)
    return nothing
end

function findlarge(sn::SnailNumber, hist::Vector{Union{Int, SnailNumber}} = Union{Int, SnailNumber}[])
    push!(hist, sn)
    if isa(sn.left, Int) && sn.left >= 10
        push!(hist, sn.left)
        return hist
    elseif !isa(sn.left, Int)
        temp = findlarge(sn.left, hist)
        if !isnothing(temp)
            return temp
        end
    end
    if isa(sn.right, Int) && sn.right >= 10
        push!(hist, sn.right)
        return hist
    elseif !isa(sn.right, Int)
        temp = findlarge(sn.right, hist)
        if !isnothing(temp)
            return temp
        end
    end
    pop!(hist)
    return nothing
end

function reduce!(sn::SnailNumber)
    toexplode = finddeep(sn, 4)
    if !isnothing(toexplode)
        lval = toexplode[end].left
        rval = toexplode[end].right
        if toexplode[end] == toexplode[end-1].left
            if isa(toexplode[end-1].right, Int)
                toexplode[end-1].right += rval
            else
                addtofirst!(toexplode[end-1].right, rval)
            end
            i = 0
            while i < length(toexplode)-1
                if toexplode[end-i] != toexplode[end-i-1].left
                    if isa(toexplode[end-i-1].left, Int)
                        toexplode[end-i-1].left += lval
                    else
                        addtolast!(toexplode[end-i-1].left, lval)
                    end
                    break
                end
                i += 1
            end
            toexplode[end-1].left = 0
        else
            if isa(toexplode[end-1].left, Int)
                toexplode[end-1].left += lval
            else
                addtolast!(toexplode[end-1].left, lval)
            end
            i = 0
            while i < length(toexplode)-1
                if toexplode[end-i] != toexplode[end-i-1].right
                    if isa(toexplode[end-i-1].right, Int)
                        toexplode[end-i-1].right += rval
                    else
                        addtofirst!(toexplode[end-i-1].right, rval)
                    end
                    break
                end
                i += 1
            end
            toexplode[end-1].right = 0
        end
        return reduce!(sn)
    end
    tosplit = findlarge(sn)
    if !isnothing(tosplit)
        n = tosplit[end]
        newsn = SnailNumber(div(n, 2), n - div(n ,2))
        if tosplit[end] == tosplit[end-1].left
            tosplit[end-1].left = newsn
        else
            tosplit[end-1].right = newsn
        end
        return reduce!(sn)
    end
    return sn
end

function reduceonce!(sn::SnailNumber)
    toexplode = finddeep(sn, 4)
    if !isnothing(toexplode)
        lval = toexplode[end].left
        rval = toexplode[end].right
        if toexplode[end] == toexplode[end-1].left
            if isa(toexplode[end-1].right, Int)
                toexplode[end-1].right += rval
            else
                addtofirst!(toexplode[end-1].right, rval)
            end
            i = 0
            while i < length(toexplode)-1
                if toexplode[end-i] != toexplode[end-i-1].left
                    if isa(toexplode[end-i-1].left, Int)
                        toexplode[end-i-1].left += lval
                    else
                        addtolast!(toexplode[end-i-1].left, lval)
                    end
                    break
                end
                i += 1
            end
            toexplode[end-1].left = 0
        else
            if isa(toexplode[end-1].left, Int)
                toexplode[end-1].left += lval
            else
                addtolast!(toexplode[end-1].left, lval)
            end
            i = 0
            while i < length(toexplode)-1
                if toexplode[end-i] != toexplode[end-i-1].right
                    if isa(toexplode[end-i-1].right, Int)
                        toexplode[end-i-1].right += rval
                    else
                        addtofirst!(toexplode[end-i-1].right, rval)
                    end
                    break
                end
                i += 1
            end
            toexplode[end-1].right = 0
        end
        return sn
    end
    tosplit = findlarge(sn)
    if !isnothing(tosplit)
        n = tosplit[end]
        newsn = SnailNumber(div(n, 2), n - div(n ,2))
        if tosplit[end] == tosplit[end-1].left
            tosplit[end-1].left = newsn
        else
            tosplit[end-1].right = newsn
        end
        return sn
    end
    return sn
end

@test parse_snailnumber("[[[[4,3],4],4],[7,[[8,4],9]]]")[1] + parse_snailnumber("[1,1]")[1] == parse_snailnumber("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")[1]

function sumList(list::Vector{T}) where T
    sum = list[1]
    for l in list[2:end]
        sum += l
    end
    return sum
end

function magnitude(n::Int)
    return n
end

function magnitude(s::SnailNumber)
    return 3 * magnitude(s.left) + 2 * magnitude(s.right)
end

function part1(input)
    list = SnailNumber[]
    for line in split(input, '\n')
        push!(list, parse_snailnumber(line)[1])
    end
    return magnitude(sumList(list))
end

@test part1(test_input) == 4140
@show part1(input)


function part2(input)
    list = SnailNumber[]
    for line in split(input, '\n')
        push!(list, parse_snailnumber(line)[1])
    end
    return maximum(magnitude(deepcopy(x) + deepcopy(y)) for x in list for y in list if x != y)
end

@test part2(test_input) == 3993
@show part2(input)
