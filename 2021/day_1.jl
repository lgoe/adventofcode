using Test
include("day_1_input.jl")

function part1(measures)
    return count(map((x, y) -> x < y, measures, measures[2:end]))
end

@test part1(test_input) == 7
@show part1(input)


function sliding_window(a, k)
    return [a[i:i+k-1] for i in 1:length(a)-k+1]
end

function part2(measures)
    window_sums = map(sum, sliding_window(measures, 3))
    return count(map((x, y) -> x < y, window_sums, window_sums[2:end]))
end

@test part2(test_input) == 5
@show part2(input)
