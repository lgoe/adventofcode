include("day_24_input.jl")

function toint(v::AbstractVector{T}) where T
    return sum(w * 10^(i-1) for (i,w) in enumerate(reverse(v)); init=0)
end

function parse_input(input)
    lines = split(input, '\n')
    A = [parse(Int, split(lines[18*(i-1)+5])[3]) for i in 1:14]
    B = [parse(Int, split(lines[18*(i-1)+6])[3]) for i in 1:14]
    C = [parse(Int, split(lines[18*(i-1)+16])[3]) for i in 1:14]
    return A, B, C
end

function doStep(z, w, A, B, C)
    x = (z % 26) + B != w ? 1 : 0
    z ÷= A
    z *= 25*x + 1
    z += (w + C) * x
    return z
end

function part1(input)
    A, B, C = parse_input(input)
    maxinput = Dict{Int, Vector{Int}}()
    maxinput[0] = []

    for i in 1:14
        maxinput_last = maxinput
        maxinput = Dict{Int, Vector{Int}}()
        for w in 1:9, z in keys(maxinput_last)
            newz = doStep(z, w, A[i], B[i], C[i])
            if newz <= prod(A[i+1:end])
                if toint([maxinput_last[z]; w]) > toint(get(maxinput, newz, []))
                    maxinput[newz] = [maxinput_last[z]; w]
                end
            end
        end
    end

    return toint(maxinput[0])
end

@show part1(input)

function part2(input)
    A, B, C = parse_input(input)
    mininput = Dict{Int, Vector{Int}}()
    mininput[0] = []

    for i in 1:14
        mininput_last = mininput
        mininput = Dict{Int, Vector{Int}}()
        for w in 1:9, z in keys(mininput_last)
            newz = doStep(z, w, A[i], B[i], C[i])
            if newz <= prod(A[i+1:end])
                if toint([mininput_last[z]; w]) < toint(get(mininput, newz, [9 for _ in 1:15]))
                    mininput[newz] = [mininput_last[z]; w]
                end
            end
        end
    end

    return toint(mininput[0])
end

@show part2(input)
