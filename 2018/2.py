import itertools
import operator
from collections import defaultdict
from functools import reduce
from hashlib import md5
from itertools import count
from math import *
from string import ascii_lowercase as lc
from string import ascii_uppercase as uc

data = open("2.in").read().splitlines()

# Part 1
# Var 1
two = 0
three = 0
for line in data:
    hasTwo, hasThree = False, False
    for char in lc:
        if line.count(char) == 2:
            hasTwo = True
        if line.count(char) == 3:
            hasThree = True
    if hasTwo:
        two += 1
    if hasThree:
        three += 1
print(two * three)

# Var 2
print(
    reduce(
        operator.mul,
        map(
            lambda num: len(
                list(
                    filter(
                        lambda line: any(
                            filter(lambda char: line.count(char) == num, lc)
                        ),
                        data,
                    )
                )
            ),
            [2, 3],
        ),
    )
)

# Part 2
# Var 1
for line1 in data:
    for line2 in data:
        diffs = len([1 for i in range(len(line1)) if line1[i] != line2[i]])
        if diffs == 1:
            common = "".join(
                [line1[i] for i in range(len(line1)) if line1[i] == line2[i]]
            )
            print(common)

# Var 2
print(
    [
        "".join([line1[i] for i in range(len(line1)) if line1[i] == line2[i]])
        for line1 in data
        for line2 in data
        if len([1 for i in range(len(line1)) if line1[i] != line2[i]]) == 1
    ][0]
)
