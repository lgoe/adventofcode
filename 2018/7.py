import itertools
import operator
import re
from collections import defaultdict
from functools import reduce
from hashlib import md5
from itertools import count
from math import *
from string import ascii_lowercase as lc
from string import ascii_uppercase as uc

data = open('7.in').read().splitlines()

lines = list(map(lambda s: list(re.findall(r'-?[A-Z]', s)), data))

finished = []
order = ""

def getchar():
    for char in uc:
        if char not in finished and len(list(filter(lambda line: line[2] == char, lines))) == 0:
            return char
    return ""

for i in range(100):
    char = getchar()
    finished.append(char)
    order = order+char
    lines = list(filter(lambda line: line[1] != char, lines))

print(order)

lines = list(map(lambda s: list(re.findall(r'-?[A-Z]', s)), data))

finished = []

worker = {(1,0):-1,(1,1):-1,(2,0):-1,(2,1):-1,(3,0):-1,(3,1):-1,(4,0):-1,(4,1):-1,(5,0):-1,(5,1):-1}
time = -1
for i in range(1000):
    done = False
    for a in range(1,int(len(worker)/2)+1):
        if worker[(a,0)] != -1:
            done = True
            worker[(a,0)] -=1
            if worker[(a,0)] == 0:
                lines = list(filter(lambda line: line[1] != worker[(a,1)], lines))
                worker[(a,0)] = -1
    for a in range(1,int(len(worker)/2)+1):
        if worker[(a,0)] == -1:
            char = getchar()
            if char != "":
                done = True
                worker[(a,1)] = char
                finished.append(worker[(a,1)])
                worker[(a,0)] = ord(worker[(a,1)])-ord('A')+61

    if done:
        time += 1

print(time)