import re
from collections import defaultdict

data = open('6.in').read().splitlines()
coords = dict(enumerate(map(lambda s: list(map(int, re.findall(r'-?\d+', s))), data)))
points = defaultdict(lambda: -1)

part2 = 0

minx, miny, maxx, maxy = 0, 0, 380, 380
for x in range(minx, maxx):
    for y in range(miny, maxy):
        dists = {i : abs(coords[i][0]-x) + abs(coords[i][1]-y) for i in range(len(coords))}
        min1 = min(dists.keys(), key=lambda i: dists[i])
        min2 = min(reversed(list(dists.keys())), key=lambda i: dists[i])
        if min1 == min2:
            points[(x,y)] = min1
        if sum(dists.values()) < 10000:
            part2 += 1
        

for x in range(minx, maxx):
    coords.pop(points[(x, miny)], None)
    coords.pop(points[(x, maxy-1)], None)

for y in range(miny, maxy):
    coords.pop(points[(minx, y)], None)
    coords.pop(points[(maxx-1, y)], None)

print(max(map(lambda i: len(list(filter(lambda p: points[p] == i, points))), coords)))
print(part2)