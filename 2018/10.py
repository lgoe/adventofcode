import itertools
import operator
import re
from collections import defaultdict
from functools import reduce
from hashlib import md5
from itertools import count
from math import *
from string import ascii_lowercase as lc
from string import ascii_uppercase as uc
import time

data = open('10.in').read().splitlines()

lines = map(lambda s: list(map(int, re.findall(r'-?\d+', s))), data)
points = {}
velocities = {}
i = 0
for line in lines:
    points[i] = line[:2]
    velocities[i] = line[2:]
    i += 1

for i in range(10950):
    draw = defaultdict(lambda: '.')
    for j in range(len(list(points.keys()))):
        points[j][0] += velocities[j][0]
        points[j][1] += velocities[j][1]
        draw[points[j][0], points[j][1]] = '#'
    if i == 10945:
        for y in range(187,199):
            s = ""
            for x in range(140,204):
                s += draw[x,y]
            print(s)
        print(i)