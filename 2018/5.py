from string import ascii_lowercase as lu

def func(b):
    data = open('5.in').read().strip().replace(b,"").replace(chr(ord(b)-32), "")
    stack = []
    for a in data:
        if stack and abs(ord(a)-ord(stack[-1])) == 32:
            stack.pop()
        else:
            stack.append(a)
    return len(stack)

print(func(" "))

ans = 1e6
for b in lu:
    ans = min(ans, func(b))

print(ans)
