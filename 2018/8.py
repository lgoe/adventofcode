import itertools
import operator
import re
from collections import defaultdict
from functools import reduce
from hashlib import md5
from itertools import count
from math import *
from string import ascii_lowercase as lc
from string import ascii_uppercase as uc

data = list(map(int, open('8.in').read().split(" ")))
sum = 0

metastack = [data[1]]
childstack = [data[0]]
valuestack = []
data = data[2:]

while childstack != []:
    num = childstack.pop()
    # print(data)
    # print(sum)
    if num == 0:
        meta = metastack.pop()
        for i in range(meta):
            sum += data[i]
        value = valuestack.pop()
        if value == "sum":
            value = 0
            for i in range(meta):
                value += data[i]
        valuestack.append(value)
        data = data[meta:]
    else:
        childstack.append(num -1)
        childstack.append(data[0])
        metastack.append(data[1])
        if data[0] == 0:
            valuestack.append("sum")
        data = data[2:]


def analyse(data):
    num = data[1]
    sum = 0
    for i in range(num):
        sum += data[-(i+1)]
    num = data[0]
    start = 2
    for i in range(num):
        end = data[start]+data[start+1]
        analyse(data[start:])
# entries = list(map(lambda s: list(map(int, re.findall(r'-?\d+', s))), data))
print(data)
print(sum)