import re
from collections import defaultdict
from itertools import count, product

data = sorted(open('4.in').read().splitlines())
times = map(lambda s: map(int, re.findall(r'-?\d+', s)), data)
guards = defaultdict(int)
minutes = defaultdict(int)
current = -1
sleeping = False
stamp = 0
for a in times:
    a = list(a)
    if len(a) == 6:
        current = a[5]
        sleeping = False
    else:
        if sleeping:
            guards[current] += a[4] - stamp
            for b in range(stamp, a[4]):
                minutes[(current, b)] += 1
        stamp = a[4]
        sleeping = not sleeping

# Part 1   
guard = max(guards.keys(), key=(lambda k: guards[k]))
out = max(filter(lambda c: c[0]==guard, minutes), key=(lambda k: minutes[k]))
print(out[0] * out[1])

# Part 2
out = max(minutes.keys(), key=(lambda k: minutes[k]))
print(out[0] * out[1])