import itertools
import operator
import re
from collections import defaultdict
from functools import reduce
from hashlib import md5
from itertools import count
from math import *
from string import ascii_lowercase as lc
from string import ascii_uppercase as uc

serial = 8141

def getLevel(x, y, serial):
    rackID = x+10
    powerLevel = rackID * y
    powerLevel += serial
    powerLevel *= rackID
    return ((powerLevel // 100) % 10) - 5

def getGridPower(tup):
    x, y, size = tup[0],tup[1],tup[2]
    sum = 0
    for dx in range(size):
        for dy in range(size):
            sum += levels[(x+dx, y+dy)]
    return sum

levels = {}
for x in range(1,301):
    for y in range(1,301):
        levels[(x,y)] = getLevel(x,y,serial)

maxkey = max([(x,y,3) for (x,y) in itertools.product(range(1,299), repeat=2)], key=getGridPower)
print(maxkey)
print(getGridPower(maxkey))   

maxkey = max([(x,y,size) for size in range(3,16) for (x,y) in itertools.product(range(1,302-size), repeat=2)], key=getGridPower)
print(maxkey)
print(getGridPower(maxkey))