import itertools
import operator
import re
from collections import defaultdict
from functools import reduce
from hashlib import md5
from itertools import count
from math import *
from string import ascii_lowercase as lc
from string import ascii_uppercase as uc

class Marble(object):
    def __init__(self, value):
        self.value = value
        self.next = self
        self.prev = self

    def add(self, value):
        if value % 23 == 0:
            rem = self
            for _ in range(7):
                rem = rem.prev
            rem.prev.next = rem.next
            rem.next.prev = rem.prev
            return rem.next, rem.value + value
        else:
            new = Marble(value)
            prev = self.next
            next = self.next.next
            prev.next = new
            next.prev = new
            new.prev = prev
            new.next = next
            return new, 0

def getScore(players, numMarbles):
    player = 1
    current = Marble(0)
    total = defaultdict(int)
    for i in range(1,numMarbles+1):
        # if i%100000 == 0:
        #     print(i)
        current, score = current.add(i)
        total[player] += score
        player = (player+1)%players
    return max(total.values())

print(getScore(476, 71657))
print(getScore(476, 7165700))